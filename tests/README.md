# Testing Strategy

## Static Testing

### Terrafom code validation
For code validation we perform two operations:

1. `terraform fmt -check -recursive` which ensures that the terraform configuration files are in a canonical format and style.

2. `terraform validate` which runs checks that verify whether a configuration is syntactically valid and internally consistent, regardless of any provided variables or existing state

### Static application security testing (SAST)
[tfsec](https://github.com/aquasecurity/tfsec) is used to perform SAST. It uses static analysis of your terraform code to spot potential misconfigurations

## Unit testing
Custom unit tests are written with [pytest](https://pytest.org) for this service. The tests parse the terraform plan output and ensure that critical infrasturecutre is not going to be destoryed during an apply

Infrastructure checked by the unit tests:

| Infrastructure Components |
| -------------- |
| Archiving S3 Bucket |
| SGW Fileshare |
| SGW Gateway |
| DataSyncAgent |
| VPC Endpoints |

## Integration Testing
Integration test is performed after the deployment. The test ensures that the test ensures that storage gateway is online and that the file share is available


