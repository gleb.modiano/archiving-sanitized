import pytest
import json
import logging
import os

# Get env variables
CI_ROOT_DIRECTORY = os.environ['CI_PROJECT_DIR']
COMPONENT = os.environ['COMPONENT']

logging.basicConfig(level=logging.DEBUG)

# Returns resources that are planned to be deleted
def get_destroyed_resources(plan):
    destroy_resources = []
    #Iterate throgh every resource and look for delete chages
    for resource in plan['resource_changes']:
        tf_resource = resource['address']
        actions = resource['change']['actions']

        for action in actions:
            if (action == 'delete'):
                destroy_resources.append(tf_resource)
            else:
                logging.debug(f"Action: {action} for resource: {tf_resource}")

    return destroy_resources    


# returns terraform plan json file for main
@pytest.fixture
def terraform_plan():
    file_path = f"{CI_ROOT_DIRECTORY}/{COMPONENT}_plan.json"
    f = open(file_path)
    plan = json.load(f)
    return plan

@pytest.fixture
def adaas_archiving_bucket():
    return 'module.s3.aws_s3_bucket.archiving_bucket'

@pytest.fixture
def adaas_archiving_sgw():
    return 'module.storage_gateway.aws_storagegateway_gateway.archiving_storage_gateway'

@pytest.fixture
def adaas_archiving_sgw_file_share():
    return 'module.storage_gateway.aws_storagegateway_nfs_file_share.archiving_storage_gateway_nfs_share'

@pytest.fixture
def adaas_archiving_datasync_agent():
    return 'module.datasync.aws_datasync_agent.datasync_agent'


def test_adaas_s3_not_deleted(adaas_archiving_bucket, terraform_plan):
    resources_to_destroy = get_destroyed_resources(terraform_plan)
    assert adaas_archiving_bucket not in resources_to_destroy

def test_adaas_archiving_sgw_file_share_not_deleted(adaas_archiving_sgw_file_share, terraform_plan):
    resources_to_destroy = get_destroyed_resources(terraform_plan)
    assert adaas_archiving_sgw_file_share not in resources_to_destroy

def test_adaas_archiving_sgw_not_deleted(adaas_archiving_sgw, terraform_plan):
    resources_to_destroy = get_destroyed_resources(terraform_plan)
    assert adaas_archiving_sgw not in resources_to_destroy

def test_adaas_archiving_datasync_agent_not_deleted(adaas_archiving_datasync_agent, terraform_plan):
    resources_to_destroy = get_destroyed_resources(terraform_plan)
    assert adaas_archiving_datasync_agent not in resources_to_destroy
