import pytest
import json
import logging
import os

# Get env variables
CI_ROOT_DIRECTORY = os.environ['CI_PROJECT_DIR']
COMPONENT = os.environ['COMPONENT']

logging.basicConfig(level=logging.DEBUG)

# Returns resources that are planned to be deleted
def get_destroyed_resources(plan):
    destroy_resources = []
    #Iterate throgh every resource and look for delete chages
    for resource in plan['resource_changes']:
        tf_resource = resource['address']
        actions = resource['change']['actions']

        for action in actions:
            if (action == 'delete'):
                destroy_resources.append(tf_resource)
            else:
                logging.debug(f"Action: {action} for resource: {tf_resource}")

    return destroy_resources    

# returns terraform plan json file for main
@pytest.fixture
def terraform_plan():
    file_path = f"{CI_ROOT_DIRECTORY}/{COMPONENT}_plan.json"
    f = open(file_path)
    plan = json.load(f)
    return plan

@pytest.fixture
def sgw_vpc_endpoint():
    return 'module.vpc.aws_vpc_endpoint.this["com.amazonaws.eu-west-2.storagegateway"]'

@pytest.fixture
def s3_vpc_endpoint():
    return 'module.vpc.aws_vpc_endpoint.this["com.amazonaws.eu-west-2.s3"]'

@pytest.fixture
def datasync_vpc_endpoint():
    return 'module.vpc.aws_vpc_endpoint.datasync_endpoint'

@pytest.fixture
def tgw_attachment():
    return 'module.vpc.aws_ec2_transit_gateway_vpc_attachment.this'


def test_sgw_vpc_endpoint_not_destroyed(sgw_vpc_endpoint, terraform_plan):
    resources_to_destroy = get_destroyed_resources(terraform_plan)
    assert sgw_vpc_endpoint not in resources_to_destroy

def test_s3_vpc_endpoint_not_destroyed(s3_vpc_endpoint, terraform_plan):
    resources_to_destroy = get_destroyed_resources(terraform_plan)
    assert s3_vpc_endpoint not in resources_to_destroy

def test_datasync_vpc_endpoint_not_destroyed(datasync_vpc_endpoint, terraform_plan):
    resources_to_destroy = get_destroyed_resources(terraform_plan)
    assert datasync_vpc_endpoint not in resources_to_destroy

def test_tgw_attachment_not_destroyed(tgw_attachment, terraform_plan):
    resources_to_destroy = get_destroyed_resources(terraform_plan)
    assert tgw_attachment not in resources_to_destroy
