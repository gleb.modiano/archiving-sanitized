# Environment
variable "environment" {
  description = "environment name"
  type        = string
}

variable "region" {
  description = "The region which the solution will be deployed in to"
  type        = string
}

#KMS
variable "archiving_key_alias_name" {
  description = "Alias for the KMS key"
  type        = string
}

# VPC
variable "vpc_id" {
  description = "VPC ID to use"
  type        = string
}

variable "subnet_ids" {
  description = "subnet ids to use"
  type        = list(string)
}

variable "datasync_sg_id" {
  description = "sg id for datasync"
  type        = string
}

variable "sgw_sg_id" {
  description = "sg id for sgw"
  type        = string
}

variable "sgw_vpc_endpoint_dns_name" {
  description = "DNS name for the sgg VPC endpoint"
  type        = string
}

variable "datasync_endpoint_ip" {
  description = "IP addess of the datasync endpoint"
  type        = string
}