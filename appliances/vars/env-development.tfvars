# Env
environment = "development"

#VPC
vpc_id = "vpc-xxxxxxxxxxxxxxxxx"
subnet_ids = [
  "subnet-xxxxxxxxxxxxxxxxx",
  "subnet-xxxxxxxxxxxxxxxxx",
  "subnet-xxxxxxxxxxxxxxxxx",
]
datasync_sg_id            = "sg-xxxxxxxxxxxxxxxxx"
sgw_sg_id                 = "sg-xxxxxxxxxxxxxxxxx"
sgw_vpc_endpoint_dns_name = "vpce-xxxxxxxxxxxxxxxxx-xxxxxxxx.storagegateway.eu-west-2.vpce.amazonaws.com"
datasync_endpoint_ip      = "0.0.0.0"
