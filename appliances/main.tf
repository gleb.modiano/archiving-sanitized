module "kms" {
  environment              = var.environment
  source                   = "./modules/kms"
  archiving_key_alias_name = var.archiving_key_alias_name
  current_region           = data.aws_region.current
  caller_identity          = data.aws_caller_identity.current
}


module "mock_appliances" {

  environment    = var.environment
  source         = "./modules/mock_appliances"
  current_region = data.aws_region.current
  subnet_ids     = var.subnet_ids
  sgw_sg_id      = var.sgw_sg_id
  datasync_sg_id = var.datasync_sg_id
  vpc_id         = var.vpc_id
  kms_key_arn    = module.kms.kms_key_arn

  sgw_vpc_endpoint_dns_name = var.sgw_vpc_endpoint_dns_name
  datasync_endpoint_ip      = var.datasync_endpoint_ip
}