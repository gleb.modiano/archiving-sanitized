variable "environment" {
  description = "environment name"
  type        = string
}

variable "subnet_ids" {
  description = "Subnet IDs for appliance and test instances"
  type        = list(string)
}

variable "vpc_id" {
  description = "ID of the archiving VPC"
  type        = string
}

variable "sgw_sg_id" {
  description = "value"
  type        = string
}

variable "datasync_sg_id" {
  description = "value"
  type        = string
}

variable "kms_key_arn" {
  description = "kms key arn"
  type        = string
}

variable "sgw_vpc_endpoint_dns_name" {
  description = "DNS name of the SGW VPC endpoint"
  type        = string
}

variable "datasync_endpoint_ip" {
  description = "IP of the datasync vpc endpoint"
  type        = string
}

variable "current_region" {
  type = any
}