data "aws_ami" "sgw_appliance_ami" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["aws-storage-gateway-*"]
  }
}

data "aws_ami" "datasync_agent_ami" {
  most_recent = true

  filter {
    name   = "name"
    values = ["aws-datasync-*"]
  }

  owners = ["amazon"]
}

data "aws_ami" "amznlinux2" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm*"]
  }

  filter {
    name   = "architecture"
    values = ["x86_64"]
  }
}

data "archive_file" "sgw_activation_lambda_code" {
  depends_on = [
    null_resource.install_dependencies
  ]
  source_dir  = "${path.module}/code/sgw_activation_lambda/_package"
  output_path = "${path.module}/sgw_activation_lambda.zip"
  type        = "zip"
}

data "archive_file" "datasync_activation_lambda_code" {
  depends_on = [
    null_resource.install_dependencies
  ]
  source_dir  = "${path.module}/code/datasync_activation_lambda/_package"
  output_path = "${path.module}/datasync_activation_lambda.zip"
  type        = "zip"
}