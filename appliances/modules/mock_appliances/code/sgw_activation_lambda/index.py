import json
import os
import requests
from requests.exceptions import ConnectionError
import sys

import boto3

SGW_GATEWAY_IP = os.environ['SGW_GATEWAY_IP']
SGW_GATEWAY_TYPE = os.environ['SGW_GATEWAY_TYPE']
SGW_ACTIVATION_REGION = os.environ['SGW_ACTIVATION_REGION']
SGW_VPC_ENDPOINT = os.environ['SGW_VPC_ENDPOINT']


def lambda_handler(event, context):
    activation_key = ''
    
    # url = 'http://10.108.48.8/?gatewayType=FILE_S3&activationRegion=eu-west-2&vpcEndpoint=vpce-0744f955804046f66-fwpwixjc.storagegateway.eu-west-2.vpce.amazonaws.com&no_redirect'
    url = 'http://{0}/?gatewayType={1}&activationRegion={2}&vpcEndpoint={3}&no_redirect'.format(SGW_GATEWAY_IP, SGW_GATEWAY_TYPE, SGW_ACTIVATION_REGION, SGW_VPC_ENDPOINT)
    
    try:
        activation_session = requests.get(url)
        activation_key = activation_session.text
        
    except requests.exceptions.ConnectionError as e:
        if activation_key == '':
            print('Failed to activate gateway - activation key unavailable')
            print(e)
            exit(13)
            
    except ConnectionRefusedError as e:
        print('Connection Refused - is the gateway activated')
        print(e)
        exit(23)
        
    except TimeoutError:
        raise ConnectTimeoutError(
            self, 'Connection to {0} timed out. (connect timeout={1})'.format(self.host, self.timeout))
            


    return {
        'statusCode': 200,
        'key': activation_key
    }



