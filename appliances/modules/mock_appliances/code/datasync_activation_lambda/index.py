import json
import os
import requests
from requests.exceptions import ConnectionError
import sys

import boto3

DATASYNC_GATEWAY_IP = os.environ['DATASYNC_GATEWAY_IP']
DATASYNC_GATEWAY_TYPE = os.environ['DATASYNC_GATEWAY_TYPE']
DATASYNC_ACTIVATION_REGION = os.environ['DATASYNC_ACTIVATION_REGION']
DATASYNC_VPC_ENDPOINT = os.environ['DATASYNC_VPC_ENDPOINT']


def lambda_handler(event, context):
    activation_key = ''
    
    url = 'http://10.108.48.22/?gatewayType=SYNC&activationRegion=eu-west-2&privateLinkEndpoint=10.108.48.25&endpointType=PRIVATE_LINK&no_redirect'
    url = 'http://{0}/?gatewayType={1}&activationRegion={2}&privateLinkEndpoint={3}&endpointType=PRIVATE_LINK&no_redirect'.format(DATASYNC_GATEWAY_IP, DATASYNC_GATEWAY_TYPE, DATASYNC_ACTIVATION_REGION, DATASYNC_VPC_ENDPOINT)
    
    try:
        activation_session = requests.get(url)
        activation_key = activation_session.text
        
    except requests.exceptions.ConnectionError as e:
        if activation_key == '':
            print('Failed to activate gateway - activation key unavailable')
            print(e)
            exit(13)
            
    except ConnectionRefusedError as e:
        print('Connection Refused - is the gateway activated')
        print(e)
        exit(23)
        
    except TimeoutError:
        raise ConnectTimeoutError(
            self, 'Connection to {0} timed out. (connect timeout={1})'.format(self.host, self.timeout))
            
    
    # ssm_client = boto3.client("ssm", region_name='eu-west-2')
    
    # parameter = ssm_client.put_parameter(
    #     # Name='/sgw/activation_key',
    #     Name=SSM_SECRET_PATH,
    #     Description='Activation key for the sgw',
    #     Value=activation_key,
    #     Type='String',
    #     Overwrite=True,
    #     Tier='Standard',
    #     DataType='text')


    return {
        'statusCode': 200,
        'key': activation_key
    }



