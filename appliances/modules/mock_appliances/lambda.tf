# Some Python lambdas have deps, we need to install and package them before deployment
resource "null_resource" "install_dependencies" {
  triggers = {
    build_number = "${timestamp()}"
  }
  provisioner "local-exec" {
    command = "bash ${path.module}/scripts/create_pkg.sh"

    environment = {
      path_module = path.module
    }
  }
}

########################################################
# Lambda to activate SGW appliance                     #
########################################################
resource "aws_iam_role" "lambda_ssm_role" {
  name               = "LambdaSsmParameterRole-${var.environment}"
  assume_role_policy = <<POLICY
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "",
            "Effect": "Allow",
            "Principal": {
                "Service": "lambda.amazonaws.com"
            },
            "Action": "sts:AssumeRole"
        }
    ]
}
POLICY
  managed_policy_arns = [
    "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole",
    "arn:aws:iam::aws:policy/service-role/AWSLambdaVPCAccessExecutionRole"
  ]
  tags = {
    "Name" = "LambdaSsmParameterRole-${var.environment}"
  }
}

#tfsec:ignore:aws-lambda-enable-tracing
resource "aws_lambda_function" "activate_sgw_function" {
  filename         = data.archive_file.sgw_activation_lambda_code.output_path
  function_name    = "ActivateMockSgwAppliance-${var.environment}"
  role             = aws_iam_role.lambda_ssm_role.arn
  handler          = "index.lambda_handler"
  source_code_hash = data.archive_file.sgw_activation_lambda_code.output_base64sha256
  runtime          = "python3.8"
  timeout          = 15
  environment {
    variables = {
      SGW_GATEWAY_IP        = aws_instance.sgw_appliance.private_ip
      SGW_GATEWAY_TYPE      = "FILE_S3"
      SGW_ACTIVATION_REGION = var.current_region.name
      SGW_VPC_ENDPOINT      = var.sgw_vpc_endpoint_dns_name
    }
  }
  vpc_config {
    subnet_ids         = [var.subnet_ids[0]]
    security_group_ids = [var.sgw_sg_id]
  }
  tags = {
    "Name" = "ActivateMockSgwAppliance-${var.environment}"
  }
}

########################################################
# Lambda to activate DataSync appliance                #
########################################################

#tfsec:ignore:aws-lambda-enable-tracing
resource "aws_lambda_function" "activate_datasync_function" {
  filename         = data.archive_file.datasync_activation_lambda_code.output_path
  function_name    = "ActivateMockDatasyncAppliance-${var.environment}"
  role             = aws_iam_role.lambda_ssm_role.arn
  handler          = "index.lambda_handler"
  source_code_hash = data.archive_file.datasync_activation_lambda_code.output_base64sha256
  runtime          = "python3.8"
  timeout          = 15
  environment {
    variables = {
      DATASYNC_GATEWAY_IP        = aws_instance.datasync_appliance.private_ip
      DATASYNC_GATEWAY_TYPE      = "SYNC"
      DATASYNC_ACTIVATION_REGION = var.current_region.name
      DATASYNC_VPC_ENDPOINT      = var.datasync_endpoint_ip
    }
  }
  vpc_config {
    subnet_ids         = [var.subnet_ids[1]]
    security_group_ids = [var.datasync_sg_id]
  }
  tags = {
    "Name" = "ActivateMockDatasyncAppliance-${var.environment}"
  }
}