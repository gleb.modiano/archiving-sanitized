output "sgw_appliance_ip" {
  description = "IP address of the SGW appliance"
  value       = aws_instance.sgw_appliance.private_ip
}