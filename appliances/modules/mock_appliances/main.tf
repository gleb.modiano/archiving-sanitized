####################################################################
# VPC Endpoint for SSM                                             #
####################################################################

#Endpoint needed by Lambda
resource "aws_vpc_endpoint" "ssm_endpoint" {
  vpc_id            = var.vpc_id
  service_name      = "com.amazonaws.${var.current_region.name}.ssm"
  subnet_ids        = [var.subnet_ids[2]]
  vpc_endpoint_type = "Interface"
  security_group_ids = [
    var.sgw_sg_id,
    var.datasync_sg_id
  ]
  private_dns_enabled = true
  tags = {
    "Name" = "com.amazonaws.${var.current_region.name}.ssm - ${var.environment}"
  }
}

# Endpoints to access the test server 
resource "aws_vpc_endpoint" "ssmmessages_endpoint" {
  vpc_id            = var.vpc_id
  service_name      = "com.amazonaws.${var.current_region.name}.ssmmessages"
  subnet_ids        = [var.subnet_ids[2]]
  vpc_endpoint_type = "Interface"
  security_group_ids = [
    var.sgw_sg_id,
    var.datasync_sg_id
  ]
  private_dns_enabled = true
  tags = {
    "Name" = "com.amazonaws.${var.current_region.name}.ssmmessages - ${var.environment}"
  }
}

resource "aws_vpc_endpoint" "ec2messages_endpoint" {
  vpc_id            = var.vpc_id
  service_name      = "com.amazonaws.${var.current_region.name}.ec2messages"
  subnet_ids        = [var.subnet_ids[2]]
  vpc_endpoint_type = "Interface"
  security_group_ids = [
    var.sgw_sg_id,
    var.datasync_sg_id
  ]
  private_dns_enabled = true
  tags = {
    "Name" = "com.amazonaws.${var.current_region.name}.ec2messages - ${var.environment}"
  }
}

####################################################################
# SGW Appliance                                                    #
####################################################################

#tfsec:ignore:aws-ec2-enforce-http-token-imds
resource "aws_instance" "sgw_appliance" {
  ami                         = data.aws_ami.sgw_appliance_ami.id
  instance_type               = "m4.xlarge"
  associate_public_ip_address = false
  subnet_id                   = var.subnet_ids[0]
  vpc_security_group_ids      = [var.sgw_sg_id]

  root_block_device {
    encrypted  = true
    kms_key_id = var.kms_key_arn
  }

  ebs_block_device {
    delete_on_termination = true
    device_name           = "/dev/sdb"
    encrypted             = true
    volume_size           = 200
    kms_key_id            = var.kms_key_arn

    tags = {
      "Name" = "SGW_Appliance_Disk-${var.environment}"
    }
  }

  tags = {
    Name = "SGW-EC2-appliance-${var.environment}"
  }
}

#tfsec:ignore:aws-iam-no-policy-wildcards
resource "aws_iam_role" "ssm_ec2_role" {
  name        = "ssm-ec2-role-${var.environment}"
  description = "The role for the developer resources EC2"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": {
  "Effect": "Allow",
  "Principal": {"Service": "ec2.amazonaws.com"},
  "Action": "sts:AssumeRole"
  }
}
EOF

  inline_policy {
    name   = "ssm-policy"
    policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "ssm:DescribeAssociation",
                "ssm:GetDeployablePatchSnapshotForInstance",
                "ssm:GetDocument",
                "ssm:DescribeDocument",
                "ssm:GetManifest",
                "ssm:GetParameter",
                "ssm:GetParameters",
                "ssm:ListAssociations",
                "ssm:ListInstanceAssociations",
                "ssm:PutInventory",
                "ssm:PutComplianceItems",
                "ssm:PutConfigurePackageResult",
                "ssm:UpdateAssociationStatus",
                "ssm:UpdateInstanceAssociationStatus",
                "ssm:UpdateInstanceInformation"
            ],
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "ssmmessages:CreateControlChannel",
                "ssmmessages:CreateDataChannel",
                "ssmmessages:OpenControlChannel",
                "ssmmessages:OpenDataChannel"
            ],
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "ec2messages:AcknowledgeMessage",
                "ec2messages:DeleteMessage",
                "ec2messages:FailMessage",
                "ec2messages:GetEndpoint",
                "ec2messages:GetMessages",
                "ec2messages:SendReply"
            ],
            "Resource": "*"
        }
    ]
}
EOF

  }

  tags = {
    "Name" = "ssm-ec2-role-${var.environment}"
  }
}

resource "aws_iam_instance_profile" "ssm_instance_profile" {
  name = "ssm-profile-${var.environment}"
  role = aws_iam_role.ssm_ec2_role.name
}

####################################################################
# DataSync Appliance                                               #
####################################################################

#tfsec:ignore:aws-ec2-enforce-http-token-imds
resource "aws_instance" "datasync_appliance" {
  ami           = data.aws_ami.datasync_agent_ami.id
  instance_type = "m5.2xlarge"

  vpc_security_group_ids      = [var.datasync_sg_id]
  subnet_id                   = var.subnet_ids[1]
  associate_public_ip_address = false

  root_block_device {
    encrypted  = true
    kms_key_id = var.kms_key_arn
  }

  tags = {
    Name = "DataSync-EC2-appliance-${var.environment}"
  }
}

####################################################################
# Testing Server                                                   #
####################################################################

#tfsec:ignore:aws-ec2-enforce-http-token-imds
resource "aws_instance" "mock_testing_server" {
  ami           = data.aws_ami.amznlinux2.id
  instance_type = "t2.micro"

  vpc_security_group_ids      = [var.datasync_sg_id, var.sgw_sg_id]
  subnet_id                   = var.subnet_ids[2]
  associate_public_ip_address = false

  iam_instance_profile = aws_iam_instance_profile.ssm_instance_profile.name

  root_block_device {
    encrypted  = true
    kms_key_id = var.kms_key_arn
  }

  tags = {
    Name = "MockTestingServer-${var.environment}"
  }
}

resource "aws_efs_file_system" "efs" {
  encrypted  = true
  kms_key_id = var.kms_key_arn

  tags = {
    Name = "MockEfsServer-${var.environment}"
  }
}

resource "aws_efs_mount_target" "alpha" {
  file_system_id = aws_efs_file_system.efs.id
  subnet_id      = var.subnet_ids[2]

  security_groups = [var.datasync_sg_id]
}

