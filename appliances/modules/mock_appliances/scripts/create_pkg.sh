#!/bin/bash

echo "Executing create_pkg.sh..."
cd $path_module/code

for d in */ ; do
  echo "Checking for dependencies in $d"
  if [ ! -f $d/requirements.txt ]; then
    echo $d/requirements.txt + " not found... skipping..."
    continue
  fi

  echo "Deleting old package..."
  rm -rf $d/_package

  echo "Creating package..."
  mkdir $d/_package
  rsync -av --progress $d $d/_package --exclude _package

  echo "Creating virtualenv..."
  # virtualenv -p python3 $d/_venv
  python3 -m venv $d/_venv
  source $d/_venv/bin/activate
  pip3 install -r $d/requirements.txt

  echo "Copying libs..."
  cp -r $d/_venv/lib/python*/site-packages/* $d/_package

  echo "Removing virtualenv..."
  rm -rf $d/_venv/
done
