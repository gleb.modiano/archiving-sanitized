variable "environment" {
  description = "environment name"
  type        = string
}

variable "archiving_key_alias_name" {
  description = "Name of the alias for the KMS key used in the archiving solution"
  type        = string
}

variable "current_region" {
  type = any
}

variable "caller_identity" {
  type = any
}