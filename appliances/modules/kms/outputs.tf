output "kms_key_arn" {
  description = "The ARN of the created KMS key to be used in the archiving solution"
  value       = aws_kms_key.archiving_kms_key.arn
}
output "kms_key_arn_alias" {
  description = "The ARN (alias) of the created KMS key to be used in the archiving solution"
  value       = aws_kms_alias.archiving_kms_key_alias.arn
}
output "kms_key_id" {
  description = "The ID of the created KMS key to be used in the archiving solution"
  value       = aws_kms_key.archiving_kms_key.id
}
