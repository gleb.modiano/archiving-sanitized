resource "aws_kms_key" "archiving_kms_key" {
  description             = "KMS key for the archiving solution"
  deletion_window_in_days = 10
  policy                  = <<POLICY
  {
    "Version": "2012-10-17",
    "Statement": [
      {
          "Sid": "Enable IAM User Permissions",
          "Effect": "Allow",
          "Principal": {
              "AWS": "arn:aws:iam::${var.caller_identity.account_id}:root"
          },
          "Action": "kms:*",
          "Resource": "*"
      },
      {
          "Sid": "Allow logs",
          "Effect": "Allow",
          "Principal": {
              "Service": "logs.${var.current_region.name}.amazonaws.com"
          },
          "Action": [
              "kms:Encrypt*",
              "kms:Decrypt*",
              "kms:ReEncrypt*",
              "kms:GenerateDataKey*",
              "kms:Describe*"
          ],
          "Resource": "*"
      },
      {
          "Sid": "Allow CloudTrail",
          "Effect": "Allow",
          "Principal": {
              "Service": "cloudtrail.amazonaws.com"
          },
          "Action": [
              "kms:Encrypt*",
              "kms:Decrypt*",
              "kms:ReEncrypt*",
              "kms:GenerateDataKey*",
              "kms:Describe*"
          ],
          "Resource": "*"
      },
      {
          "Sid": "Allow sns",
          "Effect": "Allow",
          "Principal": {
              "Service": "sns.amazonaws.com"
          },
          "Action": [
              "kms:Encrypt*",
              "kms:Decrypt*",
              "kms:ReEncrypt*",
              "kms:GenerateDataKey*",
              "kms:Describe*"
          ],
          "Resource": "*"
      },
      {
          "Sid": "Allow cloudwatch",
          "Effect": "Allow",
          "Principal": {
              "Service": "cloudwatch.amazonaws.com"
          },
          "Action": [
              "kms:Encrypt*",
              "kms:Decrypt*",
              "kms:ReEncrypt*",
              "kms:GenerateDataKey*",
              "kms:Describe*"
          ],
          "Resource": "*"
      },
      {
        "Sid": "Allow s3",
        "Effect": "Allow",
        "Principal": {"Service": "s3.amazonaws.com"},
        "Action": ["kms:GenerateDataKey*", "kms:Decrypt"],
        "Resource": "*"
      }
    ]
  }
POLICY
  tags = {
    "Name" = "ArchivingKMSKey-${var.environment}"
  }
}

resource "aws_kms_alias" "archiving_kms_key_alias" {
  depends_on    = [time_sleep.wait_for_key]
  name          = "alias/${var.archiving_key_alias_name}-${var.environment}"
  target_key_id = aws_kms_key.archiving_kms_key.id
}


resource "time_sleep" "wait_for_key" {
  create_duration = "200s"
}
