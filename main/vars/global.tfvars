# Region
region = "eu-west-2"

# S3
archiving_bucket_name = "archiving-bucket"

# SGW
storage_gateway_name                     = "ArchivingStorageGateway"
storage_gateway_nfs_allowed_clients_cidr = ["0.0.0.0/0"]

# DataSync
datasync_agent_name = "ArchivingDatasync"

# Monitoring
dashboard_name = "ArchivingDashboard"

# Restore automation
lambda_function_name = "ArchivingTriggerRestoreFunction"
role_name            = "ArchivingTriggerRestoreFunctionRole"

# Break glass
break_glass_timeout = "PT1H" # 1 hour



