# Env
environment = "development"

# VPC Settings
vpc_id = "vpc-xxxxxxxxxxxxxxxxx"
subnet_ids = [
  "subnet-xxxxxxxxxxxxxxxxx",
  "subnet-xxxxxxxxxxxxxxxxx",
  "subnet-xxxxxxxxxxxxxxxxx",
]
subnet_arns = [
  "arn:aws:ec2:eu-west-2:xxxxxxxxxxxx:subnet/subnet-xxxxxxxxxxxxxxxxx",
  "arn:aws:ec2:eu-west-2:xxxxxxxxxxxx:subnet/subnet-xxxxxxxxxxxxxxxxx",
  "arn:aws:ec2:eu-west-2:xxxxxxxxxxxx:subnet/subnet-xxxxxxxxxxxxxxxxx",
]
s3_vpc_endpoint_dns_name = "vpce-xxxxxxxxxxxxxxxxx-xxxxxxxx.s3.eu-west-2.vpce.amazonaws.com"
datasync_vpc_endpoint_id = "vpce-xxxxxxxxxxxxxxxxx"
datasync_sg_arn          = "arn:aws:ec2:eu-west-2:xxxxxxxxxxxx:security-group/sg-xxxxxxxxxxxxxxxxx"

# S3
s3_force_destroy = true

#SGW Settings
storage_gateway_disk_node = "/dev/sdb"

#DataSync
datasync_source_host_name          = "0.0.0.0"
datasync_source_sub_directory      = "/"
datasync_destination_sub_directory = "/datasync/"

# Restore automation
restore_initiated_notification_emails = ["email@email.com"]
restore_complete_notification_emails  = ["email@email.com"]

# Access analyzer
create_access_analyzer              = true
access_analyzer_notification_emails = ["email@email.com"]

# Break glass
break_glass_notification_emails = ["email@email.com"]

# KMS
kms_cmk_arn       = "arn:aws:kms:eu-west-2:xxxxxxxxxxxx:key/xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx"
kms_cmk_alias_arn = "arn:aws:kms:eu-west-2:xxxxxxxxxxxx:alias/ALIAS"