module "s3" {
  environment           = var.environment
  source                = "./modules/s3"
  archiving_bucket_name = var.archiving_bucket_name
  archiving_kms_key_arn = var.kms_cmk_arn
  current_region        = data.aws_region.current
  caller_identity       = data.aws_caller_identity.current
  s3_force_destroy      = var.s3_force_destroy
}

module "storage_gateway" {
  source                                   = "./modules/storage_gateway"
  environment                              = var.environment
  archiving_bucket_arn                     = module.s3.archiving_bucket_arn
  archiving_kms_key_arn                    = var.kms_cmk_arn
  storage_gateway_name                     = var.storage_gateway_name
  storage_gateway_activation_key           = var.storage_gateway_activation_key
  storage_gateway_nfs_allowed_clients_cidr = var.storage_gateway_nfs_allowed_clients_cidr
  storage_gateway_disk_node                = var.storage_gateway_disk_node
  current_region                           = data.aws_region.current
  caller_identity                          = data.aws_caller_identity.current
  storage_gateway_s3_dns_endpoint          = var.s3_vpc_endpoint_dns_name
}

module "datasync" {
  source                  = "./modules/datasync"
  environment             = var.environment
  datasync_agent_name     = var.datasync_agent_name
  datasync_activation_key = var.datasync_activation_key
  security_group_arns     = [var.datasync_sg_arn]
  subnet_arns             = [var.subnet_arns[1]]

  archiving_kms_key_arn              = var.kms_cmk_arn
  datasync_endpoint_id               = var.datasync_vpc_endpoint_id
  datasync_source_host_name          = var.datasync_source_host_name
  datasync_source_sub_directory      = var.datasync_source_sub_directory
  current_region                     = data.aws_region.current
  caller_identity                    = data.aws_caller_identity.current
  archiving_bucket_arn               = module.s3.archiving_bucket_arn
  datasync_destination_sub_directory = var.datasync_destination_sub_directory
}

module "restore_automation" {
  source                                = "./modules/restore_automation"
  environment                           = var.environment
  storage_gateway_log_group_name        = module.storage_gateway.storage_gateway_log_group_name
  lambda_function_name                  = var.lambda_function_name
  role_name                             = var.role_name
  bucket_name                           = module.s3.archiving_bucket_name
  restore_complete_notification_emails  = var.restore_complete_notification_emails
  restore_initiated_notification_emails = var.restore_initiated_notification_emails
  archiving_kms_key_arn                 = var.kms_cmk_arn
  current_region                        = data.aws_region.current
  caller_identity                       = data.aws_caller_identity.current
  depends_on = [
    module.s3,
    module.storage_gateway
  ]
}

module "monitoring" {
  source                = "./modules/monitoring"
  environment           = var.environment
  dashboard_name        = var.dashboard_name
  share_id              = module.storage_gateway.share_id
  gateway_id            = module.storage_gateway.gateway_id
  subnet_ids            = var.subnet_ids
  vpc_id                = var.vpc_id
  archiving_kms_key_arn = var.kms_cmk_arn
  storage_gateway_name  = var.storage_gateway_name
  bucket_name           = var.archiving_bucket_name
  current_region        = data.aws_region.current
  caller_identity       = data.aws_caller_identity.current

  depends_on = [
    module.s3,
    module.storage_gateway
  ]
}

module "access_analyzer" {
  source                              = "./modules/access_analyzer"
  environment                         = var.environment
  archiving_bucket_arn                = module.s3.archiving_bucket_arn
  access_analyzer_notification_emails = var.access_analyzer_notification_emails
  archiving_kms_key_arn               = var.kms_cmk_arn
  current_region                      = data.aws_region.current
  caller_identity                     = data.aws_caller_identity.current

  depends_on = [
    module.s3
  ]
}

module "retention_automation" {
  source                     = "./modules/retention_automation"
  environment                = var.environment
  bucket_name                = module.s3.archiving_bucket_name
  legacy_prefix              = "Legacy/"
  storage_gateway_id         = module.storage_gateway.gateway_id
  restore_complete_topic_arn = module.restore_automation.restore_complete_topic_arn
  current_region             = data.aws_region.current
  caller_identity            = data.aws_caller_identity.current
  depends_on = [
    module.s3,
    module.restore_automation
  ]
}

module "break_glass" {
  source                = "./modules/break_glass"
  environment           = var.environment
  archiving_bucket_arn  = module.s3.archiving_bucket_arn
  archiving_kms_key_arn = var.kms_cmk_arn
  break_glass_timeout   = var.break_glass_timeout
  depends_on = [
    module.s3
  ]
}

# Create the rule for break glass login in the main file, due to the fact it needs to be deployed in us-east-1
resource "aws_cloudwatch_event_rule" "break_glass_login_rule" {
  provider      = aws.us-east-1
  name          = "BreakGlassLoginRule-${var.environment}"
  event_pattern = <<EOF
{
  "source": ["aws.signin"],
  "detail-type": ["AWS Console Sign In via CloudTrail"],
  "detail": {
    "userIdentity": {
      "userName": [
        "${module.break_glass.break_glass_user_name}"
      ]
    }
  }
}
EOF
  tags = {
    "Name" = "BreakGlassLoginRule-${var.environment}"
  }
}

resource "aws_cloudwatch_event_target" "break_glass_login_target" {
  provider = aws.us-east-1
  rule     = aws_cloudwatch_event_rule.break_glass_login_rule.name
  arn      = aws_sns_topic.break_glass_login_topic.arn
}

#tfsec:ignore:aws-sns-enable-topic-encryption
resource "aws_sns_topic" "break_glass_login_topic" {
  provider     = aws.us-east-1
  display_name = "BreakGlassLoginNotificationTopic-${var.environment}"
  name         = "BreakGlassLoginNotificationTopic-${var.environment}"
  tags = {
    "Name" = "BreakGlassLoginNotificationTopic-${var.environment}"
  }
}

resource "aws_sns_topic_policy" "break_glass_login_policy" {
  provider = aws.us-east-1
  policy   = <<POLICY
{
  "Version": "2008-10-17",
  "Id": "BreakGlassLoginPolicy-${var.environment}",
  "Statement": [
    {
      "Sid": "Allow SNS access",
      "Effect": "Allow",
      "Principal": {
        "AWS": "*"
      },
      "Action": [
        "SNS:GetTopicAttributes",
        "SNS:SetTopicAttributes",
        "SNS:AddPermission",
        "SNS:RemovePermission",
        "SNS:DeleteTopic",
        "SNS:Subscribe",
        "SNS:ListSubscriptionsByTopic",
        "SNS:Publish"
      ],
      "Resource": "${aws_sns_topic.break_glass_login_topic.arn}",
      "Condition": {
        "StringEquals": {
          "AWS:SourceOwner": "${data.aws_caller_identity.current.account_id}"
        }
      }
    },
    {
      "Sid": "Allow publish to events",
      "Effect": "Allow",
      "Principal": {
        "Service": "events.amazonaws.com"
      },
      "Action": "sns:Publish",
      "Resource": "${aws_sns_topic.break_glass_login_topic.arn}"
    }
  ]
}
POLICY
  arn      = aws_sns_topic.break_glass_login_topic.arn
}

resource "aws_sns_topic_subscription" "break_glass_notification_topic_subscriptions" {
  provider  = aws.us-east-1
  topic_arn = aws_sns_topic.break_glass_login_topic.arn
  protocol  = "email"
  for_each  = toset(var.break_glass_notification_emails)
  endpoint  = each.key
}
