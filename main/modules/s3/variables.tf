variable "environment" {
  description = "environment name"
  type        = string
}

variable "s3_force_destroy" {
  description = "Boolean to force destroy S3 buckets."
  type        = bool
}

variable "archiving_bucket_name" {
  description = "The name of the archiving bucket"
  type        = string
}

variable "archiving_kms_key_arn" {
  description = "The KMS key used to encrypt the bucket"
  type        = string
}
variable "current_region" {
  type = any
}
variable "caller_identity" {
  type = any
}