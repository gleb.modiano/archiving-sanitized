data "aws_kms_key" "archiving_key" {
  key_id = var.archiving_kms_key_arn
}
