output "archiving_bucket_name" {
  value = aws_s3_bucket.archiving_bucket.bucket
}

output "archiving_bucket_arn" {
  value = aws_s3_bucket.archiving_bucket.arn
}
