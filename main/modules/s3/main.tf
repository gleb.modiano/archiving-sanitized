# First we'll create the bucket and block public access
resource "aws_s3_bucket" "archiving_bucket" {
  bucket = "${var.archiving_bucket_name}-${var.environment}"

  force_destroy = var.s3_force_destroy

  object_lock_configuration {
    object_lock_enabled = "Enabled"
  }
  lifecycle {
    ignore_changes = [
      lifecycle_rule
    ]
  }
  tags = {
    "Name" = "${var.archiving_bucket_name}-${var.environment}"
  }
}
resource "aws_s3_bucket_logging" "archiving_bucket_logging" {
  bucket = aws_s3_bucket.archiving_bucket.id

  target_bucket = aws_s3_bucket.archiving_bucket_logs.bucket
  target_prefix = "BucketLogs/"
}
resource "aws_s3_bucket_versioning" "archiving_bucket_versioning" {
  bucket = aws_s3_bucket.archiving_bucket.id
  versioning_configuration {
    status = "Enabled"
  }
}
resource "aws_s3_bucket_server_side_encryption_configuration" "archiving_bucket_sse" {
  bucket = aws_s3_bucket.archiving_bucket.bucket
  rule {
    apply_server_side_encryption_by_default {
      kms_master_key_id = data.aws_kms_key.archiving_key.arn
      sse_algorithm     = "aws:kms"
    }
  }
}
resource "aws_s3_bucket_object_lock_configuration" "archiving_bucket_object_lock" {
  bucket = aws_s3_bucket.archiving_bucket.bucket
  rule {
    default_retention {
      mode  = "GOVERNANCE"
      years = 7 # TODO: Pull out to variable
    }
  }
  lifecycle {
    ignore_changes = [
      rule
    ]
  }
}
resource "aws_s3_bucket_lifecycle_configuration" "archiving_bucket_lifecycle_config" {
  bucket     = aws_s3_bucket.archiving_bucket.bucket
  depends_on = [aws_s3_bucket_versioning.archiving_bucket_versioning]
  rule {
    id = "config"

    noncurrent_version_expiration {
      noncurrent_days = 2557
    }
    expiration {
      days = 2557
    }

    transition {
      days          = 1
      storage_class = "GLACIER_IR"
    }

    transition {
      days          = 91
      storage_class = "DEEP_ARCHIVE"
    }

    status = "Enabled"
  }
}
resource "aws_s3_bucket_public_access_block" "block_public_access_archiving_bucket" {
  bucket                  = aws_s3_bucket.archiving_bucket.id
  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

# Now we can assign a policy to the bucket to ensure only SSL requests are served and no public access on policy
resource "aws_s3_bucket_policy" "archiving_bucket_policy" {
  bucket = aws_s3_bucket.archiving_bucket.id
  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Principal": {
        "AWS": "*"
      },
      "Action": [
        "s3:*"
      ],
      "Resource": [
        "${aws_s3_bucket.archiving_bucket.arn}/*",
        "${aws_s3_bucket.archiving_bucket.arn}"
      ],
      "Effect": "Deny",
      "Condition": {
        "Bool": {
          "aws:SecureTransport": "false"
        }
      }
    },
    {
      "Principal": {
        "AWS": "*"
      },
      "Action": [
        "s3:PutObject",
        "s3:PutObjectAcl"
      ],
      "Resource": [
        "${aws_s3_bucket.archiving_bucket.arn}/*"
      ],
      "Effect": "Deny",
      "Condition": {
        "StringEquals": {
          "s3:x-amz-acl": [
            "public-read",
            "public-read-write",
            "authenticated-read"
          ]
        }
      }
    },
    {
      "Principal": {
        "AWS": "*"
      },
      "Action": [
        "s3:PutObject",
        "s3:PutObjectAcl"
      ],
      "Resource": [
        "${aws_s3_bucket.archiving_bucket.arn}/*"
      ],
      "Effect": "Deny",
      "Condition": {
        "StringLike": {
          "s3:x-amz-grant-read": [
            "*http://acs.amazonaws.com/groups/global/AllUsers*",
            "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
          ]
        }
      }
    },
    {
      "Principal": {
        "AWS": "*"
      },
      "Action": [
        "s3:PutBucketAcl"
      ],
      "Resource": [
        "${aws_s3_bucket.archiving_bucket.arn}"
      ],
      "Effect": "Deny",
      "Condition": {
        "StringEquals": {
          "s3:x-amz-acl": [
            "public-read",
            "public-read-write",
            "authenticated-read"
          ]
        }
      }
    },
    {
      "Principal": {
        "AWS": "*"
      },
      "Action": [
        "s3:PutBucketAcl"
      ],
      "Resource": [
        "${aws_s3_bucket.archiving_bucket.arn}"
      ],
      "Effect": "Deny",
      "Condition": {
        "StringLike": {
          "s3:x-amz-grant-read": [
            "*http://acs.amazonaws.com/groups/global/AllUsers*",
            "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
          ]
        }
      }
    }
  ]
}
POLICY
}

# Now let's set up CloudTrail for the archiving bucket and get it logging to another bucket
#tfsec:ignore:aws-s3-enable-bucket-logging
resource "aws_s3_bucket" "archiving_bucket_logs" {
  bucket        = "${var.archiving_bucket_name}-${var.environment}-logs"
  force_destroy = var.s3_force_destroy
  tags = {
    "Name" = "${var.archiving_bucket_name}-${var.environment}-logs"
  }
}
resource "aws_s3_bucket_server_side_encryption_configuration" "archiving_bucket_logs_sse" {
  bucket = aws_s3_bucket.archiving_bucket_logs.bucket
  rule {
    apply_server_side_encryption_by_default {
      kms_master_key_id = data.aws_kms_key.archiving_key.id
      sse_algorithm     = "aws:kms"
    }
  }
}
resource "aws_s3_bucket_versioning" "archiving_bucket_logs_versioning" {
  bucket = aws_s3_bucket.archiving_bucket_logs.id
  versioning_configuration {
    status = "Enabled"
  }
}
resource "aws_s3_bucket_public_access_block" "block_public_access_archiving_bucket_logs" {
  bucket                  = aws_s3_bucket.archiving_bucket_logs.id
  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}
resource "aws_s3_bucket_policy" "archiving_bucket_logs_policy" {
  bucket = aws_s3_bucket.archiving_bucket_logs.id
  policy = <<POLICY
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "AWSCloudTrailAclCheck",
            "Effect": "Allow",
            "Principal": {
              "Service": "cloudtrail.amazonaws.com"
            },
            "Action": "s3:GetBucketAcl",
            "Resource": "${aws_s3_bucket.archiving_bucket_logs.arn}"
        },
        {
            "Sid": "AWSCloudTrailWrite",
            "Effect": "Allow",
            "Principal": {
              "Service": "cloudtrail.amazonaws.com"
            },
            "Action": "s3:PutObject",
            "Resource": "${aws_s3_bucket.archiving_bucket_logs.arn}/AWSLogs/*",
            "Condition": {
                "StringEquals": {
                    "s3:x-amz-acl": "bucket-owner-full-control"
                }
            }
        }
    ]
}
POLICY
}
resource "aws_cloudtrail" "archiving_bucket_trail" {
  name                       = "ArchivingBucketTrail-${var.environment}"
  s3_bucket_name             = aws_s3_bucket.archiving_bucket_logs.bucket
  is_multi_region_trail      = true
  enable_log_file_validation = true
  kms_key_id                 = data.aws_kms_key.archiving_key.arn

  event_selector {
    read_write_type = "All"
    data_resource {
      type   = "AWS::S3::Object"
      values = ["${aws_s3_bucket.archiving_bucket_logs.arn}/*"]
    }
  }
  tags = {
    "Name" = "ArchivingBucketTrail-${var.environment}"
  }

  depends_on = [
    aws_s3_bucket_policy.archiving_bucket_logs_policy
  ]
}
