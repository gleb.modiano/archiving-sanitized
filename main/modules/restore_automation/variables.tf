variable "environment" {
  description = "environment name"
  type        = string
}

variable "lambda_function_name" {
  description = "The name of the Lambda function that initiates object restores"
  type        = string
}
variable "role_name" {
  description = "The name of the role which the Lambda function makes use of"
  type        = string
}
variable "bucket_name" {
  description = "The ARN of the archiving bucket"
  type        = string
}
variable "storage_gateway_log_group_name" {
  description = "The log group name where storage gateway will send its logs"
  type        = string
}
variable "restore_complete_notification_emails" {
  description = "List of email addresses that will be notified when an object restore completes"
  type        = list(string)
}

variable "restore_initiated_notification_emails" {
  description = "List of email addresses that will be notified when an object restore is initiated"
  type        = list(string)
}

variable "archiving_kms_key_arn" {
  description = "The KMS key used to encrypt the share"
  type        = string
}
variable "current_region" {
  type = any
}
variable "caller_identity" {
  type = any
}
