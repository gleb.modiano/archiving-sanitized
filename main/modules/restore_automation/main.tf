# Create roles/policies for function
resource "aws_iam_role" "trigger_restore_role" {
  name               = "${var.role_name}-${var.environment}"
  assume_role_policy = <<POLICY
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "",
            "Effect": "Allow",
            "Principal": {
                "Service": "lambda.amazonaws.com"
            },
            "Action": "sts:AssumeRole"
        }
    ]
}
POLICY
  managed_policy_arns = [
    "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
  ]
  tags = {
    "Name" = "${var.role_name}-${var.environment}"
  }
}
resource "aws_iam_role_policy" "trigger_restore_policy" {
  name   = "ArchivingS3AccessPolicy-${var.environment}"
  role   = aws_iam_role.trigger_restore_role.id
  policy = <<POLICY
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "AllowGetAndRestoration",
            "Effect": "Allow",
            "Action": [
                "s3:GetObject",
                "s3:RestoreObject",
                "s3:HeadObject"
            ],
            "Resource": [
              "${data.aws_s3_bucket.archiving_bucket.arn}",
              "${data.aws_s3_bucket.archiving_bucket.arn}/*"
            ]
        }
    ]
}
POLICY

  # tags = {
  #   "Name" = "ArchivingS3AccessPolicy--${var.environment}"
  # }
}

# Create function
#tfsec:ignore:aws-lambda-enable-tracing
resource "aws_lambda_function" "trigger_restore_function" {
  filename         = data.archive_file.trigger_restore_code.output_path
  function_name    = "${var.lambda_function_name}-${var.environment}"
  role             = aws_iam_role.trigger_restore_role.arn
  handler          = "index.lambda_handler"
  source_code_hash = data.archive_file.trigger_restore_code.output_base64sha256
  runtime          = "python3.8"
  environment {
    variables = {
      ArchiveRecallTier     = "Standard"
      DeepArchiveRecallTier = "Standard"
      RestoreDays           = "7"
    }
  }
  tags = {
    "Name" = "${var.lambda_function_name}-${var.environment}"
  }
}

# Create the lambda trigger
resource "aws_cloudwatch_log_subscription_filter" "trigger_restore_subscription" {
  name            = "ArchivingSubscriptionFilter-${var.environment}"
  log_group_name  = data.aws_cloudwatch_log_group.archiving_sgw_log_group.name
  filter_pattern  = "{ $.type = \"InaccessibleStorageClass\" }"
  destination_arn = aws_lambda_function.trigger_restore_function.arn
}
resource "aws_lambda_permission" "trigger_restore_permissions" {
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.trigger_restore_function.arn
  principal     = "logs.${var.current_region.name}.amazonaws.com"
}

# SNS topic for restore notifications
resource "aws_sns_topic" "archiving_restore_complete" {
  name              = "ArchivingRestoreComplete-${var.environment}"
  kms_master_key_id = data.aws_kms_key.archiving_key.id
  policy            = <<POLICY
{
    "Version": "2012-10-17",
    "Id": "RestoreCompleteTopicPolicy",
    "Statement": [
        {
            "Sid": "AllowS3Events",
            "Effect": "Allow",
            "Principal": {
                "Service": "s3.amazonaws.com"
            },
            "Action": [
                "SNS:Publish"
            ],
            "Resource": "arn:aws:sns:${var.current_region.name}:${var.caller_identity.account_id}:ArchivingRestoreComplete-${var.environment}",
            "Condition": {
                "ArnLike": {
                    "aws:SourceArn": "${data.aws_s3_bucket.archiving_bucket.arn}"
                },
                "StringEquals": {
                    "aws:SourceAccount": "${var.caller_identity.account_id}"
                }
            }
        }
    ]
}     
POLICY
  tags = {
    "Name" = "ArchivingRestoreComplete-${var.environment}"
  }
}

resource "aws_sns_topic_subscription" "archiving_restore_complete_subscriptions" {
  topic_arn = aws_sns_topic.archiving_restore_complete.arn
  protocol  = "email"
  for_each  = toset(var.restore_complete_notification_emails)
  endpoint  = each.key
}

# S3 event to trigger Restore Complete notification
resource "aws_s3_bucket_notification" "restore_complete_event" {
  bucket = data.aws_s3_bucket.archiving_bucket.id

  topic {
    topic_arn = aws_sns_topic.archiving_restore_complete.arn
    events    = ["s3:ObjectRestore:Completed"]
  }
}

# SNS topic for initiated restore
resource "aws_sns_topic" "archiving_restore_initiated" {
  name              = "ArchivingRestoreInitiated-${var.environment}"
  kms_master_key_id = data.aws_kms_key.archiving_key.id
  policy            = <<POLICY
{
    "Version": "2012-10-17",
    "Id": "RestoreInitiatedTopicPolicy",
    "Statement": [
        {
            "Sid": "AllowS3Events",
            "Effect": "Allow",
            "Principal": {
                "Service": "s3.amazonaws.com"
            },
            "Action": [
                "SNS:Publish"
            ],
            "Resource": "arn:aws:sns:${var.current_region.name}:${var.caller_identity.account_id}:ArchivingRestoreInitiated-${var.environment}",
            "Condition": {
                "ArnLike": {
                    "aws:SourceArn": "${data.aws_s3_bucket.archiving_bucket.arn}"
                },
                "StringEquals": {
                    "aws:SourceAccount": "${var.caller_identity.account_id}"
                }
            }
        }
    ]
}     
POLICY
  tags = {
    "Name" = "ArchivingRestoreInitiated-${var.environment}"
  }
}

resource "aws_sns_topic_subscription" "archiving_restore_initiated_subscriptions" {
  topic_arn = aws_sns_topic.archiving_restore_initiated.arn
  protocol  = "email"
  for_each  = toset(var.restore_initiated_notification_emails) # set the var
  endpoint  = each.key
}

# S3 event to trigger Restore Initiated notification
resource "aws_s3_bucket_notification" "restore_initiated_event" {
  bucket = data.aws_s3_bucket.archiving_bucket.id

  topic {
    topic_arn = aws_sns_topic.archiving_restore_initiated.arn
    events    = ["s3:ObjectRestore:Post"]
  }
}
