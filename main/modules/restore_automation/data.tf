data "aws_kms_key" "archiving_key" {
  key_id = var.archiving_kms_key_arn
}
data "aws_cloudwatch_log_group" "archiving_sgw_log_group" {
  name = var.storage_gateway_log_group_name
}
data "aws_s3_bucket" "archiving_bucket" {
  bucket = var.bucket_name
}
data "archive_file" "trigger_restore_code" {
  type = "zip"

  source {
    content  = file("${path.module}/code/trigger_restore/index.py")
    filename = "index.py"
  }

  output_path = "${path.module}/out.zip"
}