import os
import logging
import json
import boto3
import base64
import gzip
from botocore.exceptions import ClientError
import io

# Enable boto3 debug output
#boto3.set_stream_logger("")

# Set up logging
# logging.basicConfig(level=logging.INFO, format='%(levelname)s: %(asctime)s: %(message)s')
logger = logging.getLogger(__name__)
logger.setLevel('INFO')

# Create S3 Service Resource:
s3 = boto3.resource('s3')

# Define Environment Variables:
# Ensure Restore days value is integer and valid
try:
  restore_days = int(os.environ['RestoreDays'])
except ValueError:
    my_restore_days = 2

archive_restore_tier = str(os.environ['ArchiveRecallTier'])
deep_archive_restore_tier = str(os.environ['DeepArchiveRecallTier'])

# Ensure that the values set for Restore Tiers are valid and set a default value
if archive_restore_tier not in ('Expedited','Standard','Bulk'):
  archive_restore_tier = 'Expedited'
if deep_archive_restore_tier not in ('Standard', 'Bulk'):
  deep_archive_restore_tier = 'Standard'

def lambda_handler(event, context):
  cw_data = str(event['awslogs']['data'])
  # Unzip and Unpack the Data
  cw_logs = gzip.GzipFile(fileobj=io.BytesIO(base64.b64decode(cw_data))).read()
  log_events = json.loads(cw_logs)
  for log_entry in log_events['logEvents']:
    result = process_recall(log_entry)
    logger.info(result)
  return {
    'statusCode': 200,
    'body': result
    }

def process_recall(log_entry):
  logger.info("message contents: " + log_entry['message'])
  message_json = json.loads(log_entry['message'])
  if 'type' in message_json:
    logger.info("Found ErrorType")
    error_type = message_json['type']
    logger.info("ErrorType = " + error_type)
    if message_json['type'] != "InaccessibleStorageClass":
      return "Unexpected error: not related to storage class"
  else:
    return_error = "error: no type entry"
    return return_error

  if 'bucket' in message_json:
    logger.info("Found Bucket")
    s3_bucket = message_json['bucket']
    logger.info("Bucket = " + s3_bucket)
  else:
    return_error = "error: no bucket"
    return return_error

  if 'key' in message_json:
    logger.info("Found Key")
    s3_key = message_json['key']
    logger.info("Key = " + s3_key)
  else:
    return_error = "error: no key"
    return return_error

  # Create S3 Object Resource
  s3_object = s3.Object(s3_bucket, s3_key)
  try:
    obj_restore_status = s3_object.restore
    obj_storage_class = s3_object.storage_class
    obj_archive_status = s3_object.archive_status
    if obj_restore_status is None:
      logger.info('Submitting restoration request: %s' % s3_key)
      # Add support for S3-INT storage class, restore for this class does not include restore days
      # Use the user defined restore tier for Glacier and S3-INT Archive_access otherwise default to Standard
      if obj_storage_class == 'GLACIER':
        result = s3_object.restore_object(
        RestoreRequest={'Days': restore_days, 'GlacierJobParameters': {'Tier': archive_restore_tier}})
      elif obj_storage_class == 'DEEP_ARCHIVE':
        result = s3_object.restore_object(
        RestoreRequest={'Days': restore_days, 'GlacierJobParameters': {'Tier': deep_archive_restore_tier}})
      elif obj_storage_class == 'INTELLIGENT_TIERING' and obj_archive_status == 'ARCHIVE_ACCESS':
        result = s3_object.restore_object(
        RestoreRequest={'GlacierJobParameters': {'Tier': archive_restore_tier}})
      elif obj_storage_class == 'INTELLIGENT_TIERING' and obj_archive_status == 'DEEP_ARCHIVE_ACCESS':
        result = s3_object.restore_object(
        RestoreRequest={'GlacierJobParameters': {'Tier': deep_archive_restore_tier}})

    else:
      restore_message = "Restore request already submitted!"
      return restore_message
  except ClientError as e:
    return_error = "Unexpected Error whilst attempting to recall object"
    logger.error(e)
    return return_error
  return result