####################################################################
# Create the DataSync instance                                     #
####################################################################

# Create the datasync agent
resource "aws_datasync_agent" "datasync_agent" {
  name                = "${var.datasync_agent_name}-${var.environment}"
  activation_key      = var.datasync_activation_key
  security_group_arns = var.security_group_arns
  subnet_arns         = var.subnet_arns
  vpc_endpoint_id     = var.datasync_endpoint_id

  lifecycle {
    ignore_changes = [
      private_link_endpoint
    ]
  }
}

# Datasync source location
resource "aws_datasync_location_nfs" "nfs_location" {
  server_hostname = var.datasync_source_host_name
  subdirectory    = var.datasync_source_sub_directory

  on_prem_config {
    agent_arns = [aws_datasync_agent.datasync_agent.arn]
  }

  tags = {
    "Name" = "SourceLocationNFS-${var.environment}"
  }
}

# Create a role to allow DataSync to put objects in S3
#tfsec:ignore:aws-iam-no-policy-wildcards
resource "aws_iam_role" "datasync_bucket_role" {
  name               = "ArchivingDataSync-${var.current_region.name}-${var.environment}"
  assume_role_policy = <<POLICY
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Principal": {
                "Service": "datasync.amazonaws.com"
            },
            "Action": "sts:AssumeRole",
            "Condition": {
                "StringEquals": {
                    "aws:SourceAccount": "${var.caller_identity.account_id}"
                }
            }
        }
    ]
}
POLICY
  inline_policy {
    name   = "datasync-bucket-access"
    policy = <<POLICY
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": [
                "s3:GetBucketLocation",
                "s3:ListBucket",
                "s3:ListBucketMultipartUploads"
            ],
            "Effect": "Allow",
            "Resource": "${var.archiving_bucket_arn}"
        },
        {
            "Action": [
                "s3:AbortMultipartUpload",
                "s3:DeleteObject",
                "s3:GetObject",
                "s3:ListMultipartUploadParts",
                "s3:PutObject",
                "s3:GetObjectTagging",
                "s3:PutObjectTagging"
            ],
            "Effect": "Allow",
            "Resource": "${var.archiving_bucket_arn}/*"
        }
    ]
}
POLICY
  }
  tags = {
    "Name" = "ArchivingDataSyncRole-${var.current_region.name}-${var.environment}"
  }
}

# Create S3 DataSync location
resource "aws_datasync_location_s3" "s3_location" {
  s3_bucket_arn = var.archiving_bucket_arn
  subdirectory  = var.datasync_destination_sub_directory

  s3_config {
    bucket_access_role_arn = aws_iam_role.datasync_bucket_role.arn
  }

}

# Create a cloudwatch log group for the datasync task
resource "aws_cloudwatch_log_group" "datasync_logs" {
  name       = "DataSyncLogGroup-${var.environment}"
  kms_key_id = data.aws_kms_key.archiving_key.arn
}

data "aws_iam_policy_document" "datasync_logging_policy" {
  statement {
    actions = [
      "logs:CreateLogStream",
      "logs:PutLogEvents"
    ]

    resources = [
      "arn:aws:logs:*"
    ]

    principals {
      type        = "Service"
      identifiers = ["datasync.amazonaws.com"]
    }
  }
}

resource "aws_cloudwatch_log_resource_policy" "datasync_log_publishing" {
  policy_document = data.aws_iam_policy_document.datasync_logging_policy.json
  policy_name     = "datasync-log-publishing-policy-${var.environment}"
}

# Create DataSync task
resource "aws_datasync_task" "datasync_task" {
  name                     = "DataSyncTask-${var.environment}"
  source_location_arn      = aws_datasync_location_nfs.nfs_location.arn
  destination_location_arn = aws_datasync_location_s3.s3_location.arn
  cloudwatch_log_group_arn = aws_cloudwatch_log_group.datasync_logs.arn

  options {
    bytes_per_second = 2147483648
  }
}

# KMS Grant for Datasync
resource "aws_kms_grant" "Datasync_kms_grant" {
  name              = "DatasyncGrant-${var.environment}"
  key_id            = data.aws_kms_key.archiving_key.arn
  grantee_principal = aws_iam_role.datasync_bucket_role.arn
  operations        = ["Encrypt", "Decrypt", "GenerateDataKey"]
}