variable "environment" {
  description = "environment name"
  type        = string
}

variable "datasync_agent_name" {
  description = "Name of the datasync agent"
  type        = string
}

variable "datasync_activation_key" {
  description = "Activation key for the datasync agent"
  type        = string
}

variable "security_group_arns" {
  description = "Arns of SGs for datasync"
  type        = list(string)
}

variable "subnet_arns" {
  description = "Arns of subnets for the datasync"
  type        = list(string)
}

variable "datasync_endpoint_id" {
  description = "Endpoint id of the datasync vpc endpoint"
  type        = string
  default     = ""
}

variable "datasync_source_host_name" {
  description = "Specifies the IP address or DNS name of the NFS server. The DataSync Agent(s) use this to mount the NFS server."
  type        = string
}

variable "datasync_source_sub_directory" {
  description = "Subdirectory to perform actions as source. Should be exported by the NFS server"
  type        = string
}

variable "current_region" {
  type = any
}

variable "caller_identity" {
  type = any
}

variable "archiving_bucket_arn" {
  description = "value"
  type        = string
}

variable "datasync_destination_sub_directory" {
  description = "value"
  type        = string
}

variable "archiving_kms_key_arn" {
  description = "KMS key arn"
  type        = string
}
