# Create the Lambda function which will set up retention on our objects
resource "aws_iam_role" "set_retention_role" {
  name               = "ArchivingLegacyRetentionRole-${var.environment}"
  assume_role_policy = <<POLICY
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "",
            "Effect": "Allow",
            "Principal": {
                "Service": "lambda.amazonaws.com"
            },
            "Action": "sts:AssumeRole"
        }
    ]
}
POLICY
  managed_policy_arns = [
    "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
  ]
  inline_policy {
    name   = "AllowArchiveS3Access"
    policy = <<POLICY
{
    "Version": "2012-10-17",
    "Statement": [
        {
          "Effect": "Allow",
          "Action": [
            "s3:HeadObject",
            "s3:BypassGovernanceRetention",
            "s3:PutObjectRetention",
            "s3:Get*"
          ],
          "Resource": "${data.aws_s3_bucket.archiving_bucket.arn}/${var.legacy_prefix}*"
        }
    ]
}
POLICY
  }
  tags = {
    "Name" = "ArchivingLegacyRetentionRole-${var.environment}"
  }
}
#tfsec:ignore:aws-lambda-enable-tracing
resource "aws_lambda_function" "trigger_restore_function" {
  filename         = data.archive_file.set_retention_code.output_path
  function_name    = "ArchivingSetLegacyRetention-${var.environment}"
  role             = aws_iam_role.set_retention_role.arn
  handler          = "index.lambda_handler"
  source_code_hash = data.archive_file.set_retention_code.output_base64sha256
  runtime          = "python3.8"
  tags = {
    "Name" = "ArchivingSetLegacyRetention-${var.environment}"
  }
}
resource "aws_lambda_permission" "trigger_restore_permissions" {
  action         = "lambda:InvokeFunction"
  function_name  = aws_lambda_function.trigger_restore_function.arn
  principal      = "s3.amazonaws.com"
  source_arn     = data.aws_s3_bucket.archiving_bucket.arn
  source_account = var.caller_identity.account_id
}

###################################################################
# Function which removes delete markers on objects deleted by SGW #
###################################################################
resource "aws_iam_role" "delete_marker_removal_role" {
  name               = "ArchivingDeleteMarkerRemovalRole-${var.environment}"
  assume_role_policy = <<POLICY
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "",
            "Effect": "Allow",
            "Principal": {
                "Service": "lambda.amazonaws.com"
            },
            "Action": "sts:AssumeRole"
        }
    ]
}
POLICY
  managed_policy_arns = [
    "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
  ]
  inline_policy {
    name   = "AllowArchiveS3Access"
    policy = <<POLICY
{
    "Version": "2012-10-17",
    "Statement": [
        {
          "Effect": "Allow",
          "Action": [
            "s3:DeleteObject",
            "s3:DeleteObjectVersion"
          ],
          "Resource": [
            "${data.aws_s3_bucket.archiving_bucket.arn}",
            "${data.aws_s3_bucket.archiving_bucket.arn}/*"
          ]
        }
    ]
}
POLICY
  }
  tags = {
    "Name" = "ArchivingDeleteMarkerRemovalRole-${var.environment}"
  }
}
#tfsec:ignore:aws-lambda-enable-tracing
resource "aws_lambda_function" "delete_marker_removal_automation" {
  filename         = data.archive_file.delete_marker_removal_code.output_path
  function_name    = "ArchivingDeleteMarkerRemoval-${var.environment}"
  role             = aws_iam_role.delete_marker_removal_role.arn
  handler          = "index.lambda_handler"
  source_code_hash = data.archive_file.delete_marker_removal_code.output_base64sha256
  runtime          = "python3.8"
  environment {
    variables = {
      STORAGE_GATEWAY_ID = var.storage_gateway_id
    }
  }
  tags = {
    "Name" = "ArchivingDeleteMarkerRemoval-${var.environment}"
  }
}
resource "aws_lambda_permission" "delete_marker_removal_permissions" {
  action         = "lambda:InvokeFunction"
  function_name  = aws_lambda_function.delete_marker_removal_automation.arn
  principal      = "s3.amazonaws.com"
  source_arn     = data.aws_s3_bucket.archiving_bucket.arn
  source_account = var.caller_identity.account_id
  lifecycle {
    ignore_changes = [
      source_arn
    ]
  }
}

# Set up an event source so that every time a new object is added to our "legacy" folder, we trigger
resource "aws_s3_bucket_notification" "archiving_object_restore_created_notification" {
  bucket = data.aws_s3_bucket.archiving_bucket.id
  depends_on = [
    aws_lambda_permission.trigger_restore_permissions,
    aws_lambda_function.trigger_restore_function
  ]
  lambda_function {
    lambda_function_arn = aws_lambda_function.trigger_restore_function.arn
    filter_prefix       = var.legacy_prefix
    events              = ["s3:ObjectCreated:*"]
  }
  lambda_function {
    lambda_function_arn = aws_lambda_function.delete_marker_removal_automation.arn
    events              = ["s3:ObjectRemoved:DeleteMarkerCreated"]
  }
  # We have to do this here as there can only be one aws_s3_bucket_notification resource
  topic {
    topic_arn = var.restore_complete_topic_arn
    events    = ["s3:ObjectRestore:*"]
  }
  lifecycle {
    ignore_changes = [
      bucket
    ]
  }
}

# Scheduled deletion Lambda function
resource "aws_cloudwatch_event_rule" "archiving_scheduled_deletion_rule" {
  name                = "-archiving-legacy-deletion-${var.environment}"
  description         = "Runs at a fixed rate to remove expired objects from the archiving bucket"
  schedule_expression = "rate(1 day)"
  tags = {
    "Name" = "-archiving-legacy-deletion-${var.environment}"
  }
}
resource "aws_cloudwatch_event_target" "scheduled_deletion" {
  arn  = aws_lambda_function.scheduled_deletion_function.arn
  rule = aws_cloudwatch_event_rule.archiving_scheduled_deletion_rule.name
}
resource "aws_iam_role" "scheduled_deletion_role" {
  name               = "ArchivingScheduledDeletionRole-${var.environment}"
  assume_role_policy = <<POLICY
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "",
            "Effect": "Allow",
            "Principal": {
                "Service": "lambda.amazonaws.com"
            },
            "Action": "sts:AssumeRole"
        }
    ]
}
POLICY
  managed_policy_arns = [
    "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
  ]
  inline_policy {
    name   = "AllowArchiveS3Access"
    policy = <<POLICY
{
    "Version": "2012-10-17",
    "Statement": [
        {
          "Effect": "Allow",
          "Action": [
            "s3:DeleteObject",
            "s3:Get*"
          ],
          "Resource": "${data.aws_s3_bucket.archiving_bucket.arn}/${var.legacy_prefix}*"
        },
        {
          "Effect": "Allow",
          "Action": [
            "s3:List*"
          ],
          "Resource": "*"
        }
    ]
}
POLICY
  }
  tags = {
    "Name" = "ArchivingScheduledDeletionRole-${var.environment}"
  }
}
#tfsec:ignore:aws-lambda-enable-tracing
resource "aws_lambda_function" "scheduled_deletion_function" {
  filename         = data.archive_file.scheduled_delete_code.output_path
  function_name    = "ArchivingLegacyDeletion-${var.environment}"
  role             = aws_iam_role.scheduled_deletion_role.arn
  handler          = "index.lambda_handler"
  source_code_hash = data.archive_file.scheduled_delete_code.output_base64sha256
  runtime          = "python3.8"
  environment {
    variables = {
      LEGACY_PREFIX    = var.legacy_prefix
      ARCHIVING_BUCKET = data.aws_s3_bucket.archiving_bucket.bucket
    }
  }
  tags = {
    "Name" = "ArchivingLegacyDeletion-${var.environment}"
  }
}
resource "aws_lambda_permission" "trigger_scheduled_deletion_permissions" {
  action         = "lambda:InvokeFunction"
  function_name  = aws_lambda_function.trigger_restore_function.arn
  principal      = "s3.amazonaws.com"
  source_arn     = aws_cloudwatch_event_rule.archiving_scheduled_deletion_rule.arn
  source_account = var.caller_identity.account_id
  lifecycle {
    ignore_changes = [
      source_arn
    ]
  }
}
