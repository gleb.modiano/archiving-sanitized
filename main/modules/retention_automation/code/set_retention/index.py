import logging
import boto3
import os
from datetime import timedelta, datetime

META_MTIME_KEY = 'file-mtime'
RETENTION_PERIOD_DAYS = 2556.75

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

def lambda_handler(event, context):
    logger.info("Received S3 object create event - processing new objects...")
    s3_client = boto3.client('s3')
    for record in event.get('Records', []):
        s3_bucket = record.get('s3', {}).get('bucket', {}).get('name')
        s3_key = record.get('s3', {}).get('object', {}).get('key')

        # Let's make sure we get valid bucket / keys
        if not s3_bucket or not s3_key:
            logger.warn(f"Unable to process some records, got bucket={s3_bucket} and key={s3_key}")
            continue

        # Now we'll and try set the correct retention
        try:
            logger.info(f"Processing object={s3_key} in bucket={s3_bucket}")
            head_response = s3_client.head_object(
                Bucket=s3_bucket,
                Key=s3_key
            )
            object_metadata = head_response.get('Metadata')

            # Ensure the mtime key is set before we try and set retention
            if META_MTIME_KEY not in object_metadata:
                logger.warn(f"{s3_key} did not have the {META_MTIME_KEY} attribute set in metadata - skipping...")
                continue

            # Now actually set the retention
            last_modified_timestamp_ms = float(object_metadata.get(META_MTIME_KEY)[:-2]) / 1e9  # S3 stores in ns
            last_modified_datetime = datetime.utcfromtimestamp(last_modified_timestamp_ms)
            delta = datetime.now() - last_modified_datetime
            if delta.days < RETENTION_PERIOD_DAYS:
                retention_date = last_modified_datetime + timedelta(days=RETENTION_PERIOD_DAYS)
                logger.info(f"{s3_key} is still in the retention period - retaining until {retention_date}")
                logger.info(f"Setting S3 retention for {s3_key}...")
                response = s3_client.put_object_retention(
                    Bucket=s3_bucket,
                    Key=s3_key,
                    Retention={
                        'Mode': 'GOVERNANCE',
                        'RetainUntilDate': retention_date
                    },
                    BypassGovernanceRetention=True
                )
            else:
                logger.warn(f"{s3_key} is already outside the retention period - this will be deleted when the scheduled deletion runs!")
                continue
        except Exception as e:
            logger.error(f"Something happened when processing key={s3_key} bucket={s3_bucket}")
            logger.error(f"Error={e}")