import pytest
import boto3
import json
import os
import tempfile
from moto import mock_s3
from datetime import datetime, timedelta

# Set our environment variables and then import the system under test
os.environ['LEGACY_PREFIX'] = "Legacy/"
os.environ['ARCHIVING_BUCKET'] = "archiving-bucket-mocked"
import index

@pytest.fixture
def setup_fixture():
    with mock_s3():
        s3 = boto3.client('s3', region_name='eu-west-2')
        # Create the bucket
        s3.create_bucket(
            Bucket=os.environ['ARCHIVING_BUCKET'],
            CreateBucketConfiguration={'LocationConstraint': 'eu-west-2'},
            ObjectLockEnabledForBucket=True
        )

        # Setup object lock for the bucket
        s3.put_object_lock_configuration(
            Bucket=os.environ['ARCHIVING_BUCKET'],
            ObjectLockConfiguration={
                'ObjectLockEnabled': 'Enabled',
                'Rule': {
                    'DefaultRetention': {
                        'Mode': 'GOVERNANCE',
                        'Years': 7
                    }
                }
            }
        )    

        # Upload a new file to legacy
        fp = tempfile.TemporaryFile()
        fp.write(b'Hello world!')
        obj1 = f"{os.environ['LEGACY_PREFIX']}TestFile1"
        s3.upload_fileobj(fp, os.environ['ARCHIVING_BUCKET'], obj1)

        # Set it's object lock settings
        s3.put_object_retention(
            Bucket=os.environ['ARCHIVING_BUCKET'],
            Key=obj1,
            Retention={
                'Mode': 'GOVERNANCE',
                'RetainUntilDate': datetime.now() + timedelta(hours=10)  # Retain until future
            },
            BypassGovernanceRetention=True
        )

        # Upload a new file to legacy
        fp2 = tempfile.TemporaryFile()
        fp2.write(b'Hello world!')
        obj2 = f"{os.environ['LEGACY_PREFIX']}TestFile2"
        s3.upload_fileobj(fp2, os.environ['ARCHIVING_BUCKET'], obj2)

        # Set it's object lock settings
        s3.put_object_retention(
            Bucket=os.environ['ARCHIVING_BUCKET'],
            Key=obj2,
            Retention={
                'Mode': 'GOVERNANCE',
                'RetainUntilDate': datetime.now() - timedelta(hours=10)  # Retention elapsed by 10 hrs
            },
            BypassGovernanceRetention=True
        )
        yield

def test_build_objects_to_delete(setup_fixture):
    result = index.build_objects_to_delete(boto3.client('s3'))
    assert len(result) == 1
    assert result[0] == f"{os.environ['LEGACY_PREFIX']}TestFile2"