import boto3
import logging
import os
from botocore.exceptions import ClientError
from datetime import datetime

LEGACY_PREFIX = os.environ['LEGACY_PREFIX']
ARCHIVING_BUCKET = os.environ['ARCHIVING_BUCKET']

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

def get_retain_until_date(s3_client, s3key):
    response = s3_client.get_object_retention(
        Bucket=ARCHIVING_BUCKET,
        Key=s3key
    )
    try:
        return response['Retention']['RetainUntilDate']
    except KeyError:
        return datetime.strptime(response['ResponseMetadata']['HTTPHeaders']['x-amz-object-lock-retain-until-date'], "%Y-%m-%dT%H:%M:%S.%fZ")

def build_objects_to_delete(s3_client):
    paginator = s3_client.get_paginator('list_objects_v2')
    response_iterator = paginator.paginate(
        Bucket=ARCHIVING_BUCKET,
        Prefix=LEGACY_PREFIX
    )
    marked_for_deletion = []
    for page in response_iterator:
        for s3object in page.get('Contents', []):
            s3key = s3object.get('Key')
            logger.info(f"Checking {s3key}")
            try:
                retain_until_date = get_retain_until_date(s3_client, s3key)
                if datetime.now(retain_until_date.tzinfo) > retain_until_date:
                    logger.info(f"Retention has passed for {s3key} - marking for deletion")
                    marked_for_deletion.append(s3key)
                else:
                    logger.info(f"{s3key} is still in retention period - skipping")
            except ClientError as e:
                if e.response['Error']['Code'] == 'NoSuchObjectLockConfiguration':
                    logger.info(f"No object lock configuration exists for {s3key} - marking for deletion")
                    marked_for_deletion.append(s3key)
                else:
                    logger.warn(f"An unexpected error occured while processing {s3key} - skipping")
    return marked_for_deletion


def delete_marked_objects(s3_client, marked_objects):
    logger.info(f"Found {len(marked_objects)} noncurrent objects for deletion...")
    for s3object in marked_objects:
        logger.info(f"Attempting to delete {s3object}")
        try:
            response = s3_client.delete_object(
                Bucket=ARCHIVING_BUCKET,
                Key=s3object,
                BypassGovernanceRetention=False,  # Not needed, but explicitly call this out to be sure
            )
            logger.info(f"{s3object} deleted...")
        except ClientError as e:
            logger.warn(f"Unable to delete {s3object} - an unknown error occured")

def lambda_handler(event, context):
    logger.info("Running scheduled legacy deletion process...")
    s3_client = boto3.client('s3')

    # First let's find the objects without an object lock config
    marked_for_deletion = build_objects_to_delete(s3_client)

    # Now delete the objects we've found
    delete_marked_objects(s3_client, marked_for_deletion)
