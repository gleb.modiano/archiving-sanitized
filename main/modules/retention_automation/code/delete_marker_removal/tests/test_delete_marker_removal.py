import pytest
import boto3
import botocore
import os
from botocore.exceptions import ClientError
from unittest.mock import Mock, patch

# Set our SGW env var and import our system under test
os.environ['STORAGE_GATEWAY_ID'] = "sgw-1234567"
import index

@pytest.fixture
def payload_invalid_sgw_id():
    return {
        "Records": [
            {
                "userIdentity": {
                    "principalId": "Not the storage gateway"
                },
                "s3": {
                    "bucket": {
                        "name": "mys3bucket"
                    },
                    "object": {
                        "key": "123",
                        "versionId": "456"
                    }
                }
            }
        ]
    }

@pytest.fixture
def payload_valid_sgw_id():
    return {
        "Records": [
            {
                "userIdentity": {
                    "principalId": "sgw-1234567"
                },
                "s3": {
                    "bucket": {
                        "name": "mys3bucket"
                    },
                    "object": {
                        "key": "123",
                        "versionId": "456"
                    }
                }
            }
        ]
    }

@pytest.fixture
def payload_multiple_valid_sgw_id():
    return {
        "Records": [
            {
                "userIdentity": {
                    "principalId": "sgw-1234567"
                },
                "s3": {
                    "bucket": {
                        "name": "mys3bucket"
                    },
                    "object": {
                        "key": "123",
                        "versionId": "456"
                    }
                }
            },
            {
                "userIdentity": {
                    "principalId": "sgw-1234567"
                },
                "s3": {
                    "bucket": {
                        "name": "mys3bucket2"
                    },
                    "object": {
                        "key": "1232",
                        "versionId": "4562"
                    }
                }
            }
        ]
    }

@pytest.fixture
def payload_multiple_valid_and_invalid_sgw_id():
    return {
        "Records": [
            {
                "userIdentity": {
                    "principalId": "sgw-1234567"
                },
                "s3": {
                    "bucket": {
                        "name": "mys3bucket"
                    },
                    "object": {
                        "key": "123",
                        "versionId": "456"
                    }
                }
            },
            {
                "userIdentity": {
                    "principalId": "notcorrect"
                },
                "s3": {
                    "bucket": {
                        "name": "mys3bucket2"
                    },
                    "object": {
                        "key": "1232",
                        "versionId": "4562"
                    }
                }
            },
            {
                "userIdentity": {
                    "principalId": "sgw-1234567"
                },
                "s3": {
                    "bucket": {
                        "name": "mys3bucket4"
                    },
                    "object": {
                        "key": "123",
                        "versionId": "456"
                    }
                }
            },
        ]
    }
@patch("index.boto3")
def test_without_sgw_principal_delete_not_called(boto3_mock, payload_invalid_sgw_id):
    index.lambda_handler(payload_invalid_sgw_id, None)
    assert not boto3_mock.client.return_value.delete_object.called

@patch("index.boto3")
def test_with_valid_sgw_principal_delete_called(boto3_mock, payload_valid_sgw_id):
    index.lambda_handler(payload_valid_sgw_id, None)
    boto3_mock.client.return_value.delete_object.assert_called_once_with(
        Bucket=payload_valid_sgw_id['Records'][0]['s3']['bucket']['name'],
        Key=payload_valid_sgw_id['Records'][0]['s3']['object']['key'],
        VersionId=payload_valid_sgw_id['Records'][0]['s3']['object']['versionId']
    )

@patch("index.boto3")
def test_delete_marker_removal_with_multiple_payloads(boto3_mock, payload_multiple_valid_sgw_id):
    index.lambda_handler(payload_multiple_valid_sgw_id, None)
    assert boto3_mock.client.return_value.delete_object.call_count == 2
    
@patch("index.boto3")
def test_delete_marker_removal_valid_and_invalid_payloads(boto3_mock, payload_multiple_valid_and_invalid_sgw_id):
    index.lambda_handler(payload_multiple_valid_and_invalid_sgw_id, None)
    assert boto3_mock.client.return_value.delete_object.call_count == 2

def test_with_malformed_payload_handled_gracefully():
    index.lambda_handler({"Records": True}, None)  # Unhandled exception here will fail the test