import boto3
import os
import json
import logging
from botocore.exceptions import ClientError

SGW_ID = os.environ.get('STORAGE_GATEWAY_ID')

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

def lambda_handler(event, context):
    try:
        for record in event.get('Records', []):
            if SGW_ID not in record['userIdentity'].get('principalId'):
                continue
            
            bucket = record['s3']['bucket']['name']
            object_key = record['s3']['object']['key']
            version_id = record['s3']['object']['versionId']
            logger.info(f"Detected delete marker was added by Storage Gateway - attempting to remove delete marker...")
            logger.info(f"bucket={bucket} key={object_key} delete_marker_version_id={version_id}")
            client = boto3.client('s3')
            try:
                client.delete_object(
                    Bucket=bucket,
                    Key=object_key,
                    VersionId=version_id,
                )
            except ClientError as e:
                logger.error(f"Failed to delete marker for {object_key}! Error: {str(e)}")
            logger.info("...done")
    except Exception as e:
        logger.error(f"Failed to iterate delete markers! Error: {str(e)}")