variable "environment" {
  description = "environment name"
  type        = string
}

variable "bucket_name" {
  description = "The name of the storage bucket"
  type        = string
}
variable "legacy_prefix" {
  description = "The folder which legacy (migrated) objects will be stored in"
  type        = string
}

variable "restore_complete_topic_arn" {
  description = "The topic ARN to trigger when a restore is completed"
  type        = string
}

variable "storage_gateway_id" {
  description = "The ID of the storage gateway"
  type        = string
}
variable "current_region" {
  type = any
}
variable "caller_identity" {
  type = any
}