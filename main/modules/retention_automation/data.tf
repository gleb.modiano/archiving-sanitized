data "aws_s3_bucket" "archiving_bucket" {
  bucket = var.bucket_name
}
data "archive_file" "set_retention_code" {
  type = "zip"
  source {
    content  = file("${path.module}/code/set_retention/index.py")
    filename = "index.py"
  }
  output_path = "${path.module}/set_retention.zip"
}
data "archive_file" "scheduled_delete_code" {
  type = "zip"
  source {
    content  = file("${path.module}/code/scheduled_delete/index.py")
    filename = "index.py"
  }
  output_path = "${path.module}/scheduled_delete.zip"
}
data "archive_file" "delete_marker_removal_code" {
  type = "zip"
  source {
    content  = file("${path.module}/code/delete_marker_removal/index.py")
    filename = "index.py"
  }
  output_path = "${path.module}/delete_marker_removal.zip"
}