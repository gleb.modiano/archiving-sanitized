variable "environment" {
  description = "environment name"
  type        = string
}

variable "access_analyzer_notification_emails" {
  description = "The list of emails to notify when a new access analyzer finding is created"
  type        = list(string)
}
variable "archiving_bucket_arn" {
  description = "ARN of the archiving bucket"
  type        = string
}
variable "archiving_kms_key_arn" {
  description = "The KMS key used to encrypt the share"
  type        = string
}
variable "current_region" {
  type = any
}
variable "caller_identity" {
  type = any
}