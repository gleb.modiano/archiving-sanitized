# Terraform does not support filter rules, so in this case we'll use CFN to deploy
resource "aws_cloudformation_stack" "access_analyzer_stack" {
  name          = "ArchivingAnalyzer-${var.environment}"
  template_body = file("${path.module}/cloudformation/analyzer_cfn.yml")
  parameters = {
    "Workspace" = var.environment
  }
  tags = {
    "Name" = "ArchivingAnalyzer-${var.environment}"
  }
}

# Now we'll create the SNS topic to notify when a new finding is created
resource "aws_sns_topic" "archiving_access_analyzer" {
  name              = "AccessAnalyzerFinding-${var.environment}"
  kms_master_key_id = data.aws_kms_key.archiving_key.id
  policy            = <<POLICY
{
    "Version":"2012-10-17",
    "Statement":[{
        "Effect": "Allow",
        "Principal": { "Service": "events.amazonaws.com" },
        "Action": "SNS:Publish",
        "Resource": "arn:aws:sns:*:*:AccessAnalyzerFinding"
    }]
}
POLICY
  tags = {
    "Name" = "AccessAnalyzerFinding-${var.environment}"
  }
}
resource "aws_sns_topic_subscription" "archiving_access_analyzer_subscriptions" {
  topic_arn = aws_sns_topic.archiving_access_analyzer.arn
  protocol  = "email"
  for_each  = toset(var.access_analyzer_notification_emails)
  endpoint  = each.key
}

# And finally, create the event that triggers the SNS topic
resource "aws_cloudwatch_event_rule" "archiving_access_analyzer_rule" {
  name        = "ArchivingAccessAnalyzerEventTrigger-${var.environment}"
  description = "Triggers an SNS topic when a new access analyzer finding is created against our archiving bucket"

  event_pattern = <<EOF
{
  "source": ["aws.access-analyzer"],
  "detail-type": ["Access Analyzer Finding"],
  "resources": ["${var.archiving_bucket_arn}"]
}
EOF
  tags = {
    "Name" = "ArchivingAccessAnalyzerEventTrigger-${var.environment}"
  }
}

resource "aws_cloudwatch_event_target" "archiving_access_analyzer_target" {
  arn  = aws_sns_topic.archiving_access_analyzer.arn
  rule = aws_cloudwatch_event_rule.archiving_access_analyzer_rule.name
}
