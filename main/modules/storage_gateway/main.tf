# First we'll set up a log group for storage gateway to log to
resource "aws_cloudwatch_log_group" "archiving_storage_gateway_log_group" {
  name       = "${var.storage_gateway_name}-${var.environment}-log-group"
  kms_key_id = data.aws_kms_key.archiving_key.arn
  tags = {
    "Name" = "${var.storage_gateway_name}-${var.environment}-log-group"
  }
}

# And a role for the fileshare to assume
#tfsec:ignore:aws-iam-no-policy-wildcards
resource "aws_iam_role" "storage_gateway_bucket_role" {
  name               = "ArchivingStorageGatewayRole-${var.current_region.name}-${var.environment}"
  assume_role_policy = <<POLICY
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Principal": {
                "Service": "storagegateway.amazonaws.com"
            },
            "Action": "sts:AssumeRole",
            "Condition": {
                "StringEquals": {
                    "aws:SourceAccount": "${var.caller_identity.account_id}",
                    "aws:SourceArn": "${aws_storagegateway_gateway.archiving_storage_gateway.arn}"
                }
            }
        }
    ]
}
POLICY
  inline_policy {
    name   = "storage-gateway-bucket-access"
    policy = <<POLICY
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": [
                "s3:GetAccelerateConfiguration",
                "s3:GetBucketLocation",
                "s3:GetBucketVersioning",
                "s3:ListBucket",
                "s3:ListBucketVersions",
                "s3:ListBucketMultipartUploads"
            ],
            "Resource": "${var.archiving_bucket_arn}",
            "Effect": "Allow"
        },
        {
            "Action": [
                "s3:AbortMultipartUpload",
                "s3:DeleteObject",
                "s3:DeleteObjectVersion",
                "s3:GetObject",
                "s3:GetObjectAcl",
                "s3:GetObjectVersion",
                "s3:ListMultipartUploadParts",
                "s3:PutObject",
                "s3:PutObjectAcl"
            ],
            "Resource": "${var.archiving_bucket_arn}/*",
            "Effect": "Allow"
        }
    ]
}
POLICY
  }
  tags = {
    "Name" = "ArchivingStorageGatewayRole-${var.current_region.name}-${var.environment}"
  }
}

# Now set up the storage gateway itself
# resource "aws_storagegateway_gateway" "archiving_storage_gateway" {
#   activation_key       = var.storage_gateway_activation_key
#   gateway_name         = var.storage_gateway_name
#   gateway_timezone     = "GMT"
#   gateway_type         = "FILE_S3"
#   gateway_vpc_endpoint = "com.amazonaws.${var.current_region.name}.storagegateway"

#   lifecycle {
#     ignore_changes = [
#       gateway_vpc_endpoint
#     ]
#   }

#   tags = {
#     "Name" = "${var.storage_gateway_name}-${var.environment}"
#   }
# }

resource "aws_storagegateway_gateway" "archiving_storage_gateway" {
  activation_key           = var.storage_gateway_activation_key
  gateway_name             = "${var.storage_gateway_name}-${var.environment}"
  gateway_timezone         = "GMT"
  gateway_type             = "FILE_S3"
  gateway_vpc_endpoint     = "com.amazonaws.${var.current_region.name}.storagegateway"
  cloudwatch_log_group_arn = aws_cloudwatch_log_group.archiving_storage_gateway_log_group.arn

  lifecycle {
    ignore_changes = [
      gateway_vpc_endpoint
    ]
  }

  tags = {
    "Name" = "${var.storage_gateway_name}-${var.environment}"
  }
}

# Set up local cache
resource "aws_storagegateway_cache" "archiving_storage_gateway_cache" {
  disk_id     = data.aws_storagegateway_local_disk.archiving_storage_gateway_disk.id
  gateway_arn = aws_storagegateway_gateway.archiving_storage_gateway.arn
  lifecycle {
    ignore_changes = [
      disk_id
    ]
  }
}

# Set up the file share
resource "aws_cloudwatch_log_group" "archiving_storage_gateway_nfs_log_group" {
  name       = "${var.storage_gateway_name}-${var.environment}-nfs-audit-log-group"
  kms_key_id = data.aws_kms_key.archiving_key.arn
  tags = {
    "Name" = "${var.storage_gateway_name}-${var.environment}-nfs-audit-log-group"
  }
}

resource "aws_storagegateway_nfs_file_share" "archiving_storage_gateway_nfs_share" {
  client_list           = var.storage_gateway_nfs_allowed_clients_cidr
  gateway_arn           = aws_storagegateway_gateway.archiving_storage_gateway.arn
  location_arn          = var.archiving_bucket_arn
  role_arn              = aws_iam_role.storage_gateway_bucket_role.arn
  kms_encrypted         = true
  kms_key_arn           = data.aws_kms_key.archiving_key.arn
  audit_destination_arn = aws_cloudwatch_log_group.archiving_storage_gateway_nfs_log_group.arn
  vpc_endpoint_dns_name = var.storage_gateway_s3_dns_endpoint
  bucket_region         = var.current_region.name
  cache_attributes {
    cache_stale_timeout_in_seconds = 300
  }
  tags = {
    "Name" = "ArchivingNfsFileShare-${var.environment}"
  }
}

# We'll need to grant our role access to the key / cloudwatch
resource "aws_kms_grant" "archiving_storage_gateway_kms_grant" {
  name              = "StorageGatewayGrant-${var.environment}"
  key_id            = data.aws_kms_key.archiving_key.arn
  grantee_principal = aws_iam_role.storage_gateway_bucket_role.arn
  operations        = ["Encrypt", "Decrypt", "GenerateDataKey"]
}

