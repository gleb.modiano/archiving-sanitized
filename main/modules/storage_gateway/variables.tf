variable "environment" {
  description = "environment name"
  type        = string
}

# Input from other modules
variable "archiving_bucket_arn" {
  description = "The bucket ARN used to back the storage gateway"
  type        = string
}
variable "archiving_kms_key_arn" {
  description = "The KMS key used to encrypt the share"
  type        = string
}

# Storage gateway specific ARNs
variable "storage_gateway_name" {
  description = "The name given to the storage gateway"
  type        = string
  default     = "mynewstoragegateway"
}

variable "storage_gateway_activation_key" {
  description = "The activation key for the storage gateway appliance"
  type        = string
}

variable "storage_gateway_nfs_allowed_clients_cidr" {
  description = "The CIDR ranges of clients allowed to access the nfs share"
  type        = list(string)
}

variable "storage_gateway_disk_node" {
  description = "The disk node to use for the local cache (eg. /dev/sdb)"
  type        = string
}

variable "storage_gateway_s3_dns_endpoint" {
  description = "The DNS name for the S3 VPC endpoint (used to create the file share)"
  type        = string
}

variable "current_region" {
  type = any
}
variable "caller_identity" {
  type = any
}
