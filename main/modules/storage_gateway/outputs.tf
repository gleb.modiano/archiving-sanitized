output "storage_gateway_role_arn" {
  description = "The role that storage gateway assumes"
  value       = aws_iam_role.storage_gateway_bucket_role.arn
}

output "gateway_id" {
  description = "The gateway ID"
  value       = aws_storagegateway_gateway.archiving_storage_gateway.gateway_id
}

output "share_id" {
  description = "The share ID"
  value       = aws_storagegateway_nfs_file_share.archiving_storage_gateway_nfs_share.fileshare_id
}

output "storage_gateway_log_group_name" {
  description = "The name of the log group where storage gateway logs"
  value       = aws_cloudwatch_log_group.archiving_storage_gateway_log_group.name
}
