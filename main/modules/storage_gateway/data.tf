data "aws_storagegateway_local_disk" "archiving_storage_gateway_disk" {
  disk_node   = var.storage_gateway_disk_node
  gateway_arn = aws_storagegateway_gateway.archiving_storage_gateway.arn
}
data "aws_kms_key" "archiving_key" {
  key_id = var.archiving_kms_key_arn
}