resource "aws_iam_user" "break_glass_iam_user" {
  name = "archiving-break-glass-${var.environment}"
  tags = {
    "Name" = "archiving-break-glass-${var.environment}"
  }
}
#tfsec:ignore:aws-iam-no-policy-wildcards
resource "aws_iam_policy" "break_glass_policy" {
  name        = "ArchivingBreakGlassPolicy-${var.environment}"
  path        = "/"
  description = "The break-glass user policy allows actions on the archiving S3 bucket to retreive files."

  policy = <<POLICY
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "s3:Get*",
                "s3:List*",
                "s3:RestoreObject"
            ],
            "Resource": [
                "${var.archiving_bucket_arn}",
                "${var.archiving_bucket_arn}/*"
            ],
            "Condition": {"Bool": {"aws:MultiFactorAuthPresent": "true"}}
        },
        {
            "Effect": "Allow",
            "Action": [
                "s3:ListAllMyBuckets"
            ],
            "Resource": "*",
            "Condition": {"Bool": {"aws:MultiFactorAuthPresent": "true"}}
        },
        {
          "Effect": "Allow",
          "Action": [
              "kms:Encrypt*",
              "kms:Decrypt*",
              "kms:ReEncrypt*",
              "kms:GenerateDataKey*",
              "kms:Describe*"
          ],
          "Resource": "${var.archiving_kms_key_arn}",
          "Condition": {"Bool": {"aws:MultiFactorAuthPresent": "true"}}
        }
    ]
}
POLICY
  tags = {
    "Name" = "ArchivingBreakGlassPolicy-${var.environment}"
  }
}

resource "aws_kms_grant" "break_glass_kms_grant" {
  name              = "BreakGlassGrant-${var.environment}"
  key_id            = data.aws_kms_key.archiving_key.arn
  grantee_principal = aws_iam_user.break_glass_iam_user.arn
  operations        = ["Encrypt", "Decrypt", "GenerateDataKey"]
}

resource "aws_ssm_document" "enable_break_glass_document" {
  name          = "enable-break-glass-user-${var.environment}"
  content       = <<EOF
{
  "description" : "Run this document to enable the break glass user for a period of time, after the timer has elapsed the user will have policies removed.",
  "schemaVersion" : "0.3",
  "mainSteps" : [ {
    "name" : "attach_policy",
    "action" : "aws:executeAwsApi",
    "inputs" : {
      "Service" : "iam",
      "Api" : "AttachUserPolicy",
      "PolicyArn" : "${aws_iam_policy.break_glass_policy.arn}",
      "UserName" : "${aws_iam_user.break_glass_iam_user.name}"
    },
    "description" : "Attaches the required policy to the break-glass user."
  }, {
    "name" : "sleep",
    "action" : "aws:sleep",
    "inputs" : {
      "Duration" : "${var.break_glass_timeout}"
    }
  }, {
    "name" : "remove_policy",
    "action" : "aws:executeAwsApi",
    "description" : "Removes the user policy from the break-glass account.",
    "inputs" : {
      "Service" : "iam",
      "Api" : "DetachUserPolicy",
      "UserName" : "${aws_iam_user.break_glass_iam_user.name}",
      "PolicyArn" : "${aws_iam_policy.break_glass_policy.arn}"
    }
  } ]
}
EOF
  document_type = "Automation"
  tags = {
    "Name" = "enable-break-glass-user-${var.environment}"
  }
}
