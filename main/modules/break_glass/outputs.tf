output "break_glass_user_name" {
  description = "The IAM user name of the break glass user"
  value       = aws_iam_user.break_glass_iam_user.name
}
