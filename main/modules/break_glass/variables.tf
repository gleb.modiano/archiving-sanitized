variable "environment" {
  description = "environment name"
  type        = string
}

variable "archiving_bucket_arn" {
  description = "The bucket ARN used to back the storage gateway"
  type        = string
}
variable "archiving_kms_key_arn" {
  description = "The KMS key used to encrypt the share"
  type        = string
}
variable "break_glass_timeout" {
  description = "The duration the break glass user will have permissions (in ISO8601 format)"
  type        = string
}
