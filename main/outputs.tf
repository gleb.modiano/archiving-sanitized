output "storage_gateway_id" {
  value = module.storage_gateway.gateway_id
}
output "nfs_share_id" {
  value = module.storage_gateway.share_id
}