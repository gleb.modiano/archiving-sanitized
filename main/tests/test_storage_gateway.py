import pytest
import boto3
import json

def get_nfs_arn_from_id(id, gateway_arn, client):
    paginator = client.get_paginator('list_file_shares')
    response_iterator = paginator.paginate(
        GatewayARN=gateway_arn
    )
    for page in response_iterator:
        for fs in page.get('FileShareInfoList', []):
            if fs.get('FileShareId') == id:
                return fs.get('FileShareARN')
    return None

def get_sgw_arn_from_id(id, client):
    paginator = client.get_paginator('list_gateways')
    response_iterator = paginator.paginate()
    for page in response_iterator:
        for sgw in page.get('Gateways', []):
            if sgw.get('GatewayId') == id:
                return sgw.get('GatewayARN')
    return None

@pytest.fixture
def terraform_outputs():
    with open('outputs.json', 'r') as file:
        return json.loads(file.read())
    return None

@pytest.fixture
def storagegateway_client():
    return boto3.client('storagegateway')

def test_gateway_up(terraform_outputs, storagegateway_client):
    """
    Tests if the Storage Gateway appliance is showing as up from the AWS side
    """
    sgw_id = terraform_outputs['storage_gateway_id']['value']
    sgw_arn = get_sgw_arn_from_id(sgw_id, storagegateway_client)
    response = storagegateway_client.describe_gateway_information(
        GatewayARN=sgw_arn
    )
    assert response['GatewayState'] == 'RUNNING'

def test_nfs_file_share_available(terraform_outputs, storagegateway_client):
    """
    Ensures the file share is AVAILABLE. If this test fails it either indicates an issue
    with the network connectivity to the S3 endpoint, or potentially a bucket policy.
    """
    sgw_id = terraform_outputs['storage_gateway_id']['value']
    sgw_arn = get_sgw_arn_from_id(sgw_id, storagegateway_client)
    nfs_share_id = terraform_outputs['nfs_share_id']['value']
    nfs_share_arn = get_nfs_arn_from_id(nfs_share_id, sgw_arn, storagegateway_client)
    response = storagegateway_client.describe_nfs_file_shares(
        FileShareARNList=[
            nfs_share_arn,
        ]
    )
    assert response['NFSFileShareInfoList'][0]['FileShareStatus'] == 'AVAILABLE'