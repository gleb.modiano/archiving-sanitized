# Environment
variable "environment" {
  description = "environment name"
  type        = string
}

# VPC 
variable "vpc_id" {
  description = "ID of the VPC to use"
  type        = string
}

variable "subnet_ids" {
  description = "IDs of subnets in the VPC"
  type        = list(string)
}

variable "subnet_arns" {
  description = "ARNs of VPC subnets"
  type        = list(string)
}

variable "s3_vpc_endpoint_dns_name" {
  description = "DNS name of S3 VPC endpoint"
  type        = string
}

variable "datasync_vpc_endpoint_id" {
  description = "ID of the datasync endpoint"
  type        = string
}

variable "datasync_sg_arn" {
  description = "ARN of the datasync security group"
  type        = string
}

# S3
variable "archiving_bucket_name" {
  description = "The name of the archiving bucket"
  type        = string
}

variable "s3_force_destroy" {
  description = "Boolean to force destroy S3 buckets."
  type        = bool
}

# Storage gateway
variable "storage_gateway_name" {
  description = "The name given to the storage gateway"
  type        = string
}

# TODO: Decide on this!!!
variable "storage_gateway_activation_key" {
  description = "The activation key for the storage gateway appliance"
  type        = string
  default     = "XXXXX-XXXXX-XXXXX-XXXXX-XXXXX"
}

variable "storage_gateway_nfs_allowed_clients_cidr" {
  description = "The CIDR ranges of clients allowed to access the nfs share"
  type        = list(string)
}

variable "storage_gateway_disk_node" {
  description = "The disk node to use for the local cache (eg. /dev/sdb)"
  type        = string
  # default     = "SCSI (0:1)"
}

#DataSync
variable "datasync_agent_name" {
  description = "Name of the datasync agent"
  type        = string
  default     = "ArchivingDatasync"

}

# TODO: Decide on this!!!
variable "datasync_activation_key" {
  description = "Activation key for the datasync agent"
  type        = string
  default     = "XXXXX-XXXXX-XXXXX-XXXXX-XXXXX"
}

variable "datasync_source_host_name" {
  description = "Specifies the IP address or DNS name of the NFS server. The DataSync Agent(s) use this to mount the NFS server."
  type        = string
  default     = "0.0.0.0"
}

variable "datasync_source_sub_directory" {
  description = "Subdirectory to perform actions as source. Should be exported by the NFS server"
  type        = string
  default     = "/"
}

variable "datasync_destination_sub_directory" {
  description = "value"
  type        = string
  default     = "/"
}

# KMS
variable "archiving_key_alias_name" {
  description = "Name of the alias for the KMS key used in the archiving solution"
  type        = string
  default     = "ArchivingKey"
}

# Monitoring
variable "dashboard_name" {
  description = "What to name the cloudwatch dashboard"
  type        = string
}


# Restore automation
variable "lambda_function_name" {
  description = "The name of the Lambda function that initiates object restores"
  type        = string
}
variable "role_name" {
  description = "The name of the role which the Lambda function makes use of"
  type        = string
}
variable "restore_complete_notification_emails" {
  description = "List of email addresses that will be notified when an object restore completes"
  type        = list(string)
  default     = ["email@email.com"]
}

variable "restore_initiated_notification_emails" {
  description = "List of email addresses that will be notified when an object restore is initiated"
  type        = list(string)
  default     = ["email@email.com"]
}

# Access analyzer
variable "create_access_analyzer" {
  description = "Create access analyzer?"
  type        = bool
}

variable "access_analyzer_notification_emails" {
  description = "The list of emails to notify when a new access analyzer finding is created"
  type        = list(string)
  default     = ["email@email.com"]
}

# Region
variable "region" {
  description = "The region which the solution will be deployed in to"
  type        = string
}

# Break glass
variable "break_glass_timeout" {
  description = "The duration the break glass user will have permissions (in ISO8601 format)"
  type        = string
}

variable "break_glass_notification_emails" {
  type        = list(any)
  description = "The list of email addresses which will receive a notification when the break glass user logs in"
  default     = ["email@email.com"]
}

variable "kms_cmk_arn" {
  description = "ARN of the CMK to use"
  type        = string
  default     = "arn:aws:kms:eu-west-2:xxxxxxxxxxxx:key/xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx"
}

variable "kms_cmk_alias_arn" {
  description = "Alias ARN of the CMK to use"
  type        = string
  default     = "arn:aws:kms:eu-west-2:xxxxxxxxxxxx:alias/ALIAS"
}