# Env
environment = "development"

# VPC Settings
vpc_cidr          = "0.0.0.0/0"
tgw_id            = "tgw-xxxxxxxxxxxxxxxxx"
subnet_size       = 28
prefix_list_id    = "pl-xxxxxxxxxxxxxxxxx"
requested_subnets = 3