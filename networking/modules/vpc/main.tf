####################################################################
# Create the VPC and associated resources for the SGW endpoint VPC #
####################################################################
resource "aws_vpc" "this" {
  cidr_block           = var.vpc_cidr
  enable_dns_support   = true
  enable_dns_hostnames = true
  tags = {
    Name = "ArchivingVpc-${var.environment}"
  }
}

resource "aws_subnet" "this" {
  count                   = local.requested_subnets
  cidr_block              = local.split_subnet[count.index]
  map_public_ip_on_launch = false
  vpc_id                  = aws_vpc.this.id
  availability_zone       = data.aws_availability_zones.available.names[count.index % local.total_zones]
  tags = {
    Name = "Private Subnet ${count.index} ${var.environment}"
  }
}

resource "aws_route_table" "this" {
  count  = local.requested_subnets
  vpc_id = aws_vpc.this.id

  dynamic "route" {
    for_each = local.cidr_list
    content {
      cidr_block         = route.value
      transit_gateway_id = var.tgw_id
    }
  }

  route {
    destination_prefix_list_id = var.prefix_list_id
    transit_gateway_id         = var.tgw_id
  }

  tags = {
    Name = "Private Route Table ${count.index} ${var.environment}"
  }
}

resource "aws_route_table_association" "this" {
  count          = local.requested_subnets
  subnet_id      = aws_subnet.this[count.index].id
  route_table_id = aws_route_table.this[count.index].id
}

resource "aws_security_group" "storage_gw_sg" {
  name        = "StorageGatewaySg-${var.environment}"
  description = "Allow inbound traffic for Storage Gateway"
  vpc_id      = aws_vpc.this.id

  dynamic "ingress" {
    for_each = local.ingress_rules
    content {
      description = ingress.value.description
      from_port   = ingress.value.from_port
      to_port     = ingress.value.to_port
      protocol    = ingress.value.protocol
      cidr_blocks = ingress.value.cidr_blocks
    }
  }

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    self        = true
    description = "Allows traffic within the security group"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["10.0.0.0/8"]
    description = "Allows egress"
  }
  tags = {
    "Name" = "StorageGatewaySg-${var.environment}"
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_security_group" "datasync_sg" {
  name        = "DataSyncSg-${var.environment}"
  description = "Allow traffic for AWS DataSync"
  vpc_id      = aws_vpc.this.id

  dynamic "ingress" {
    for_each = local.datasync_ingress_rules
    content {
      description = ingress.value.description
      from_port   = ingress.value.from_port
      to_port     = ingress.value.to_port
      protocol    = ingress.value.protocol
      cidr_blocks = ingress.value.cidr_blocks
    }
  }

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    self        = true
    description = "Allows traffic within the security group"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["10.0.0.0/8"]
    description = "Allows egress"
  }

  tags = {
    "Name" = "DataSyncSg-${var.environment}"
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_vpc_endpoint" "datasync_endpoint" {
  vpc_id            = aws_vpc.this.id
  service_name      = "com.amazonaws.${var.current_region.name}.datasync"
  subnet_ids        = aws_subnet.this[*].id
  vpc_endpoint_type = "Interface"
  security_group_ids = [
    aws_security_group.datasync_sg.id,
  ]
  private_dns_enabled = false
  tags = {
    "Name" = "VPC endpoint for DataSync - ${var.environment}"
  }
}


resource "aws_vpc_endpoint" "this" {
  for_each          = toset(local.interface_endpoints)
  vpc_id            = aws_vpc.this.id
  service_name      = each.value
  subnet_ids        = aws_subnet.this[*].id
  vpc_endpoint_type = "Interface"
  security_group_ids = [
    aws_security_group.storage_gw_sg.id,
  ]
  private_dns_enabled = false
  tags = {
    "Name" = "VPC endpoint for ${each.value} - ${var.environment}"
  }
}

################################################
# Attach transit gateway and set up the routes #
################################################
resource "aws_ec2_transit_gateway_vpc_attachment" "this" {
  subnet_ids                                      = aws_subnet.this[*].id
  transit_gateway_id                              = var.tgw_id
  vpc_id                                          = aws_vpc.this.id
  transit_gateway_default_route_table_association = false
  transit_gateway_default_route_table_propagation = false
  lifecycle {
    ignore_changes = [
      transit_gateway_default_route_table_association,
      transit_gateway_default_route_table_propagation
    ]
  }
}
