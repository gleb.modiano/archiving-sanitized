output "vpc_endpoints" {
  value = aws_vpc_endpoint.this[*]
}

output "subnet_ids" {
  value = [for k, v in aws_subnet.this : aws_subnet.this[k].id]
}

output "subnet_arns" {
  value = [for k, v in aws_subnet.this : aws_subnet.this[k].arn]
}

output "vpc_id" {
  value = aws_vpc.this.id
}

output "datasync_sg_arn" {
  value = [aws_security_group.datasync_sg.arn]
}

output "datasync_sg_id" {
  value = [aws_security_group.datasync_sg.id]
}

output "sgw_sg_arn" {
  value = [aws_security_group.storage_gw_sg.arn]
}

output "sgw_sg_id" {
  value = [aws_security_group.storage_gw_sg.id]
}

output "datasync_endpoint_id" {
  value = aws_vpc_endpoint.datasync_endpoint.id
}

output "network_ids_datasync_endpoint" {
  value = aws_vpc_endpoint.datasync_endpoint.network_interface_ids
}
