variable "environment" {
  description = "environment name"
  type        = string
}

variable "vpc_cidr" {
  type        = string
  description = "The CIDR range of the overarching VPC"
}

variable "tgw_id" {
  type        = string
  description = "The ID of the transit gateway that we'll attach to"
}

variable "prefix_list_id" {
  type        = string
  description = "The ID of the prefix list which routes"
}

variable "subnet_size" {
  type        = number
  description = "The size of the private subnets (in CIDR notation)"
}
variable "current_region" {
  type = any
}

variable "requested_subnets" {
  description = "number of subnets to build"
  type        = number
}
