locals {
  # TODO: REFACTOR ME -> Use vars!
  cidr_list = [
    "10.0.0.0/8",
  ]
  ingress_rules = [
    {
      description = "TLS"
      from_port   = 443
      to_port     = 443
      protocol    = "tcp"
      cidr_blocks = ["10.0.0.0/8"]
    },
    {
      description = "SGW"
      from_port   = 1026
      to_port     = 1028
      protocol    = "tcp"
      cidr_blocks = ["10.0.0.0/8"]
    },
    {
      description = "SGW"
      from_port   = 1031
      to_port     = 1031
      protocol    = "tcp"
      cidr_blocks = ["10.0.0.0/8"]
    },
    {
      description = "SGW"
      from_port   = 2222
      to_port     = 2222
      protocol    = "tcp"
      cidr_blocks = ["10.0.0.0/8"]
    }
  ]

  datasync_ingress_rules = [
    {
      description = "HTTP"
      from_port   = 80
      to_port     = 80
      protocol    = "tcp"
      cidr_blocks = ["10.0.0.0/8"]
    },
    {
      description = "DATASYNC"
      from_port   = 1024
      to_port     = 1064
      protocol    = "tcp"
      cidr_blocks = ["10.0.0.0/8"]
    },
    {
      description = "DATASYNC_HTTPS"
      from_port   = 443
      to_port     = 443
      protocol    = "tcp"
      cidr_blocks = ["10.0.0.0/8"]
    }
  ]
}

locals {
  subnet_diff = var.subnet_size - tonumber(split("/", var.vpc_cidr)[1])
  split_subnet = tolist([
    for i in range(local.requested_subnets) :
    cidrsubnet(var.vpc_cidr, local.subnet_diff, i)
  ])
  requested_subnets = var.requested_subnets
  interface_endpoints = [
    "com.amazonaws.${var.current_region.name}.s3",
    "com.amazonaws.${var.current_region.name}.storagegateway",
  ]
}
locals {
  total_zones = length(data.aws_availability_zones.available.names)
}
