# Environment
variable "environment" {
  description = "environment name"
  type        = string
}

# Region
variable "region" {
  description = "The region which the solution will be deployed in to"
  type        = string
}

# VPC 
variable "vpc_cidr" {
  description = "CIDR range of the VPC"
  type        = string
}

variable "tgw_id" {
  description = "CIDR range of the VPC"
  type        = string
}

variable "subnet_size" {
  description = "CIDR range of the VPC"
  type        = string
}

variable "prefix_list_id" {
  description = "CIDR range of the VPC"
  type        = string
}

variable "requested_subnets" {
  description = "number of subnets to build"
  type        = number
}
