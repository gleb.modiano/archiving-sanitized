module "vpc" {
  environment       = var.environment
  source            = "./modules/vpc"
  vpc_cidr          = var.vpc_cidr
  tgw_id            = var.tgw_id
  subnet_size       = var.subnet_size
  prefix_list_id    = var.prefix_list_id
  current_region    = data.aws_region.current
  requested_subnets = var.requested_subnets
}